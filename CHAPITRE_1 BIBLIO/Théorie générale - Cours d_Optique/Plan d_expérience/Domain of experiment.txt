Le premier niveau d'analyse consiste à prendre un paramètre de sortie pour plusieurs paramètres d'entrée. 

Yi = f(X1, X2, X3, ...) associé à une fonction polynomiale

Les variables d'entrées doivent être normalisées pour devenir comparables. 

Xi = 2 * (Pi - <Pi>) / minmax(Pi)
ici le 2 vient du fait qu'on centre la loi sur 0 puis on la normalize cette variation sur la demi gamme complete. 

var(Pi) = minmax(Pi)/2
