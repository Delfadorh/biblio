Deuxième cours GSON
	
	Espérenace:
		Moyenne empirique : ~x = 1/n Somme{xi}{i; n}

		Règle de calcul des Espérenaces : 

			E(a+b) = E(a) + E(b)
			E(a.x) = a.E(x)

			E( Somme{ai.xi} ) = Somme{ a.E(xi) }

	Estimateur de Variance : 

		~s = 1/n Somme{(xi - ~x)^2}
		L'estimateur est biaisé car le nombre de différences entre chaque valeur et la moyenne est égale à n-1. 

		V(X) = E((X-E(X))^2) = E(X^2) - E(X)^2 > 0
		regles calul pour variance :
		
			V(X + Y)= V(X) + V(Y) + 2*Cov(X,Y)
			
			V( Somme( ai.xi ) ) = Sommei( Sommej( ai.aj.Cov(Xi, Xj)))			
	
	Estimateur de la covariance :
		Cov(X,Y) = E( (x-E(x)*(y-E(y) )
	
	
	
