# Import of graphical classes
from tkinter import filedialog
from tkinter import messagebox
from tkinter import Frame, Entry, Label, Canvas, Button, Menu, Checkbutton, StringVar, IntVar, DoubleVar, LEFT, RIGHT, NW, Tk, ALL
import tkinter as tk
from tkinter import ttk
from PIL import Image, ImageTk
import ast
import numpy as np

# Import basic tools
import os
import datetime
import traceback
import sys
import time
import threading

import modules.img_utils as imgfx
import Lasers.CO2_Taufenbach.API_taufenbach_co2 as co2


def read_app_config():
    """
    This function will read the default values of the App
    Opens the App default options
    :return: dict of options
    """
    f = open("defaults.txt", 'r')
    app_config = f.read()
    app_config = ast.literal_eval(app_config)
    f.close()
    root = os.getcwd().replace('\\','/')
    for key in app_config['paths']:
        if not key == "user_document":
            app_config['paths'][key] = root + app_config['paths'][key]

    if not os.path.exists(os.path.expanduser(app_config['paths']["user_document"])):
        os.mkdir(os.path.expanduser(app_config['paths']["user_document"]))

    return app_config
# def write_laser_config(dic):
#     """
#     This function is meant to write a new laser config. It is not used is the scope.
#     :param dic: the parameters to write
#     :return: None
#     """
#     f = open(laser_config_filepath, 'r')
#     content = f.read()
#     f.close()
#     init_config = ast.literal_eval(content)
#     for key in init_config.keys():
#         init_config[key] = dic[key]
#
#     string_list = str(init_config).replace(',',',\r').replace('{', '{\r ').replace('}', '\r}')
#     f = open(laser_config_filepath, 'w')
#     f.write(str(string_list))
#     f.close()
def read_img(filepath, x, y):
    """
    This function will open the image ans resize it to the desired format.
    This function doesn't keep the aspect ratio.
    :param filepath: the filepath of the image
    :param x: width og the image in px
    :param y: height of the image in px
    :return: the resized PIL.Image
    """
    image = Image.open(filepath)
    image = image.resize((round(x),round(y)))
    return image


ROOT = os.getcwd().replace("\\", '/')
NOW = datetime.datetime.strftime(datetime.datetime.now(), '%Y%m%d_%H-%M-%S')
APP = read_app_config()
DEBUG = True


def App_config():
    window = tk.Tk()
    window.title("Select a Laser")
    LASER = {}

    def callback_validate_laser():
        if len(laser_select_widget.curselection()) == 1:
            selected_laser = lasers[laser_select_widget.curselection()[0]]
            laser_root = APP['paths']['laser_config_path'] + '/' + selected_laser

            f = open(laser_root + "/laser_config.txt", 'r')
            content = f.read()
            f.close()
            LASER = ast.literal_eval(content)
            co2.CONFIG = LASER

            window.destroy()


    #Mandatory commands
    lasers = os.listdir(APP['paths']['laser_config_path'])

    laser_select_widget = tk.Listbox(window, width=30, height=9, selectmode='single')
    laser_select_widget.yview_scroll(1, 'units')
    laser_select_widget.pack()
    for laser in lasers:
        laser_select_widget.insert(tk.END, laser)

    Button(window, text="Validate", command=callback_validate_laser).pack(side=tk.BOTTOM)
    window.mainloop()

    return LASER

class Popup():
    """
    This object is supposed to manage parameters of the application objects.
    The main object is a tkinter.TopLevel() that can be resized and filled with the desired widgets.
    function manage_... receives the object to modify and format the parameters in widgets.
    callback_valider_... checks the parameters entered in the widget, puts the values from the widgets "tkvar" into the "value" parameter of each options
    fill_... manages the display of options in the same Frame. One frame can be filled with different content.

    """
    def __init__(self, main_window):

        self.window = tk.Toplevel(main_window)
        self.window.focus()
        self.window.grab_set()
        self.window.resizable(False, False)
        self.lab = 0
        self.img_filepath = ""

    # Add pen windows
    def manage_pen(self, pen_object, action):
        #Window
        self.new_pen_window = self.window
        self.new_pen_window.geometry("390x500")
        self.new_pen_window.wm_title("Ajouter un nouveau pen")
        
        #Variables
        self.pen = pen_object
        self.support_list = []
        self.manage_pen_action = action
    

        if action == "modify":
           self.pen.val_to_tk()

        #Titre
        titre = Label(self.new_pen_window, text="Fenêtre de création de Pen")
        titre.configure(font=("", 16, "bold"))
        titre.place(x=2,y=2)

        # Param Grid
        grid_frame = Frame(self.new_pen_window)
        grid_frame.place(x=2, y=30)
        lab_width = 15
        entry_width=10

        for row, key in enumerate(self.pen.dic.keys()):
            tktype = self.pen.dic[key]['type']

            Label(grid_frame, text=self.pen.dic[key]["name"], anchor='w', width=lab_width).grid(row=row)

            if key == "pen_number":
                Entry(grid_frame, textvariable=self.pen.dic[key]["tkvar"], width=entry_width, state=tk.DISABLED).grid(row=row,  column=1)
            else:
                if tktype in ['int', 'float', 'string']:
                    Entry(grid_frame, textvariable=self.pen.dic[key]['tkvar'], width=entry_width).grid(row=row, column=1)
                elif tktype == "bool":
                    Checkbutton(grid_frame, variable=self.pen.dic[key]["tkvar"], width=entry_width).grid(row=row, column=1)
                elif tktype == 'list':
                    temp_frame = Frame(grid_frame)
                    temp_frame.grid(row=row, column=1)
                    tk.Radiobutton(temp_frame, variable=self.pen.dic[key]["tkvar"], text="S", value=self.pen.dic[key]['values'][0]).pack(side='left')
                    tk.Radiobutton(temp_frame, variable=self.pen.dic[key]["tkvar"], text="Z", value=self.pen.dic[key]['values'][1]).pack(side='right')

            Label(grid_frame, text=self.pen.dic[key]["unit"], anchor='w', width=lab_width).grid(row=row, column=2)


        # Buttons
        Button(self.new_pen_window, text="Valider", command=self.callback_valider_pen, width = 15).place(x=180+90, y=450+20)
        Button(self.new_pen_window, text="Annluer", command=self.callback_cancel, width=15).place(x=80+90, y=450+20)
        Button(self.new_pen_window, text="Import", command=self.callback_choose_pen, width=15).place(x=180+90,y=35)
        Button(self.new_pen_window, text="Save", command=self.callback_save_pen, width=15).place(x=180+90,y=60)
    def callback_valider_pen(self):
        out = self.pen.check_values()
        if self.pen.is_valid:
            self.pen.tk_to_val()
            app.update_pen(self.pen, self.manage_pen_action)
            self.new_pen_window.destroy()
            del self
        else:
            self.raise_error(out)
    def callback_choose_pen(self):
        pen_filepath = filedialog.askopenfilename(filetypes=[('PEN', '.pen')],
                                                  initialdir=APP['paths']["saving_pen_path"])
        if pen_filepath == "":
            return
        out = self.pen.from_file(pen_filepath)
    def callback_save_pen(self):
        f = filedialog.asksaveasfile(title="Save Pen as",
                                     mode='w',
                                     defaultextension=".pen",
                                     filetypes=[('PEN', '.pen')],
                                     initialdir=APP['paths']["saving_pen_path"])
        if f is None:  # asksaveasfile return `None` if dialog closed with "cancel".
            return
        self.pen.to_file(f)

    # Add Figure window
    def manage_figure(self, figure_object, action):
        self.figure = figure_object
        self.manage_figure_action = action
        if action == "modify" or action == "add+":
            self.figure.val_to_tk()

        # Window
        self.new_fig_window = self.window
        self.new_fig_window.geometry("520x395")
        self.new_fig_window.wm_title("Ajouter une nouvelle Figure")

        #Titre et widgets
        titre = Label(self.new_fig_window, text="Fenêtre de création de Figure")
        titre.configure(font=("", 16, "bold"))
        titre.place(x=2, y=2)

        # final add or cancel buttons
        Button(self.new_fig_window, text="Valider", command=self.callback_valider_figure, width=10).place(x=100, y=350)
        Button(self.new_fig_window, text="Annuler", command=self.new_fig_window.destroy, width=10).place(x=20, y=350)
        Frame(self.new_fig_window, background="grey", height=350).place(x=200, y=40)

        # Params grid frame
        grid_frame = Frame(self.new_fig_window)
        grid_frame.place(x=2, y=40)
        Label(grid_frame, text=self.figure.dic['fig_number']['name'], width=12, anchor='w').grid(row=0)
        Label(grid_frame, text=self.figure.dic['fig_custom_name']['name'], width=12, anchor='w').grid(row=1)
        Label(grid_frame, text=self.figure.dic['fig_type']['name'], width=12, anchor='w').grid(row=2)
        Entry(grid_frame, textvariable=self.figure.dic['fig_number']['tkvar'], width=15, state=tk.DISABLED).grid(row=0, column=1)
        Entry(grid_frame, textvariable=self.figure.dic['fig_custom_name']['tkvar'], width=15).grid(row=1, column=1)
        self.figure_type_widget = ttk.Combobox(grid_frame, textvariable=self.figure.dic['fig_type']['tkvar'], width=7)
        self.figure_type_widget['values'] = self.figure.dic["fig_type"]["values"]
        self.figure_type_widget['state'] = 'readonly'
        self.figure_type_widget.grid(row=2, column=1)
        self.figure_type_widget.bind('<<ComboboxSelected>>', self.fig_type_select)

        # Declare Vector and image frames
        self.image_frame_right = Frame(self.new_fig_window)
        self.image_frame_bottom = Frame(self.new_fig_window, width="180", height="180")
        self.vector_frame_right = Frame(self.new_fig_window, background='black')
        self.vector_frame_bottom = Frame(self.new_fig_window, width=150, height=150, bg='black')

        # Filll the figure param window with the good frame
        if action == 'add':
            self.figure_type_widget.set(self.figure.dic['fig_type']['value'])#Choose figure type by default (vecteur)
            self.fig_type_select("add")

        if action == "modify" or action == "add+":
            fig_type = self.figure.dic['fig_type']['value']
            if fig_type == "image":
                self.figure_type_widget.set("image")
                self.fig_type_select(action)

            elif fig_type == 'vecteur':
                self.figure_type_widget.set("vecteur")
                self.fig_type_select(action)
            else: print("problème interne")
    def callback_valider_figure(self):
        out = self.figure.check_values()
        if self.figure.is_valid:
            self.figure.tk_to_val()
            self.figure.import_image(self.img_filepath)
            self.new_fig_window.destroy()
            app.update_figure(self.figure, self.manage_figure_action)
            del self
        else:
            self.raise_error(out)
    def fill_vector_frame(self, action):

        # Vector frame bottom
        photo = ImageTk.PhotoImage(read_img(APP['paths']['vector_img'], 150, 150))
        self.lab = Label(self.vector_frame_bottom, image=photo, anchor=tk.CENTER)
        self.lab.image = photo
        self.lab.place(x=0,y=0)

        # Vector Frame Right
        grid_frame = Frame(self.vector_frame_right)

        vector_param = ["v_position_X", "v_position_Y", "v_length", "v_angle", "v_repeat_angle", "v_repeat_length"]
        width_lab = 17
        width_entry = 12
        Label(grid_frame, text="Vecteur", width=width_lab, anchor="w", font=("", 10, "bold")).grid(row=1)
        Label(grid_frame, text=self.figure.dic["v_position_X"]["name"], width=width_lab, anchor='w').grid(row=2)
        Label(grid_frame, text=self.figure.dic["v_position_Y"]["name"], width=width_lab, anchor='w').grid(row=3)
        Label(grid_frame, text=self.figure.dic["v_length"]["name"], width=width_lab, anchor='w').grid(row=4)
        Label(grid_frame, text=self.figure.dic["v_angle"]["name"], width=width_lab, anchor='w').grid(row=5)
        Label(grid_frame, text="Répétition", width=width_lab, anchor='w', font=("", 10, "bold")).grid(row=6)
        Label(grid_frame, text=self.figure.dic["v_repeat_angle"]["name"], width=width_lab, anchor='w').grid(row=7)
        Label(grid_frame, text=self.figure.dic["v_repeat_length"]["name"], width=width_lab, anchor='w').grid(row=8)
        Label(grid_frame, text=self.figure.dic["v_repeat_number"]["name"], width=width_lab, anchor='w').grid(row=9)
        
        Entry(grid_frame, textvariable=self.figure.dic["v_position_X"]["tkvar"], width=width_entry).grid(row=2, column=1)
        Entry(grid_frame, textvariable=self.figure.dic["v_position_Y"]["tkvar"], width=width_entry).grid(row=3, column=1)
        Entry(grid_frame, textvariable=self.figure.dic["v_length"]["tkvar"], width=width_entry).grid(row=4, column=1)
        Entry(grid_frame, textvariable=self.figure.dic["v_angle"]["tkvar"], width=width_entry).grid(row=5, column=1)
        Entry(grid_frame, textvariable=self.figure.dic["v_repeat_angle"]["tkvar"], width=width_entry).grid(row=7, column=1)
        Entry(grid_frame, textvariable=self.figure.dic["v_repeat_length"]["tkvar"], width=width_entry).grid(row=8, column=1)
        Entry(grid_frame, textvariable=self.figure.dic["v_repeat_number"]["tkvar"], width=width_entry).grid(row=9, column=1)

        Label(grid_frame, text=self.figure.dic["v_position_X"]["unit"], width=width_entry).grid(row=2, column=2)
        Label(grid_frame, text=self.figure.dic["v_position_Y"]["unit"], width=width_entry).grid(row=3, column=2)
        Label(grid_frame, text=self.figure.dic["v_length"]["unit"], width=width_entry).grid(row=4, column=2)
        Label(grid_frame, text=self.figure.dic["v_angle"]["unit"], width=width_entry).grid(row=5, column=2)
        Label(grid_frame, text=self.figure.dic["v_repeat_angle"]["unit"], width=width_entry).grid(row=7, column=2)
        Label(grid_frame, text=self.figure.dic["v_repeat_length"]["unit"], width=width_entry).grid(row=8, column=2)

        grid_frame.pack()
    def fill_img_frame(self, action):


        # Define image_frame_bottom
        Button(self.image_frame_bottom, text="Import", command=self.choose_img, width=24).pack(side=tk.TOP)

        #Frame right varialbes and tkvariables
        figure_vars = ["position_X", 'position_Y', 'fig_rotation', 'size_X', 'size_Y']
        width_lab = 17
        width_entry = 12


        Label(self.image_frame_right, text="Coordonnées", width=width_lab, anchor="w", font=("", 10, "bold")).grid(row=1)
        Label(self.image_frame_right, text=self.figure.dic["position_X"]["name"], width=width_lab, anchor="w").grid(row=2)
        Label(self.image_frame_right, text=self.figure.dic["position_Y"]["name"], width=width_lab, anchor="w").grid(row=3)
        Label(self.image_frame_right, text=self.figure.dic["fig_rotation"]["name"], width=width_lab, anchor="w").grid(row=4)
        Label(self.image_frame_right, text="Taille", width=width_lab, anchor="w", font=("", 10, "bold")).grid(row=5)
        Label(self.image_frame_right, text=self.figure.dic["size_X"]["name"], width=width_lab, anchor="w").grid(row=6)
        Label(self.image_frame_right, text=self.figure.dic["size_Y"]["name"], width=width_lab, anchor="w").grid(row=7)

        Entry(self.image_frame_right, textvariable=self.figure.dic["position_X"]["tkvar"], width=width_entry).grid(row=2, column=1)
        Entry(self.image_frame_right, textvariable=self.figure.dic["position_Y"]["tkvar"], width=width_entry).grid(row=3, column=1)
        Entry(self.image_frame_right, textvariable=self.figure.dic["fig_rotation"]["tkvar"], width=width_entry).grid(row=4, column=1)
        Entry(self.image_frame_right, textvariable=self.figure.dic["size_X"]["tkvar"], width=width_entry).grid(row=6, column=1)
        Entry(self.image_frame_right, textvariable=self.figure.dic["size_Y"]["tkvar"], width=width_entry).grid(row=7, column=1)

        Label(self.image_frame_right, text=self.figure.dic["position_X"]["unit"],width=width_entry, anchor="w").grid(row=2, column=2)
        Label(self.image_frame_right, text=self.figure.dic["position_Y"]["unit"], width=width_entry, anchor="w").grid(row=3, column=2)
        Label(self.image_frame_right, text=self.figure.dic["fig_rotation"]["unit"], width=width_entry, anchor="w").grid(row=4, column=2)
        Label(self.image_frame_right, text=self.figure.dic["size_X"]["unit"], width=width_entry, anchor="w").grid(row=6, column=2)
        Label(self.image_frame_right, text=self.figure.dic["size_Y"]["unit"], width=width_entry, anchor="w").grid(row=7, column=2)
        if action == 'modify' or action == "add+":
            self.choose_img(action)
    def fig_type_select(self, handler_or_action):

        # Display or Hide the right Frame
        if self.figure_type_widget.get() == "image":
            # Unfill the vector frame
            for child in self.vector_frame_bottom.winfo_children():
                child.destroy()
            for child in self.vector_frame_right.winfo_children():
                child.destroy()
            self.vector_frame_right.place_forget()
            self.vector_frame_bottom.place_forget()

            # Fill and display the img frame
            self.fill_img_frame(handler_or_action)
            self.image_frame_bottom.place(x=10, y=130)
            self.image_frame_right.place(x=210, y=40)



        elif self.figure_type_widget.get() == 'vecteur':
            # Unfill the image frame
            for child in self.image_frame_bottom.winfo_children():
                child.destroy()
            for child in self.image_frame_right.winfo_children():
                child.destroy()
            self.image_frame_right.place_forget()
            self.image_frame_bottom.place_forget()

            # Fill and display the vecteur frame
            self.fill_vector_frame(handler_or_action)
            self.vector_frame_bottom.place(x=20, y=160)
            self.vector_frame_right.place(x=210, y=40)




        else:
            return
    def choose_img(self, action=None):
        # action == 'add'
        if action == None:
            self.img_filepath = filedialog.askopenfilename(filetypes =[('PNG', '.png')])
            if self.img_filepath == "":
                return
            self.img_filename = self.img_filepath.split("/")[-1]
            self.figure.import_image(self.img_filepath)

            if self.lab != 0:
                self.lab.destroy()

            photo = ImageTk.PhotoImage(self.figure.icon)
            self.lab = Label(self.image_frame_bottom, image=photo, anchor=tk.CENTER)
            self.lab.image = photo
            self.lab.pack(side=tk.BOTTOM)

        elif action == 'modify' or action == "add+":
            if self.lab != 0:
                self.lab.destroy()

            if self.figure.icon != 0:
                photo = ImageTk.PhotoImage(self.figure.icon)
                self.lab = Label(self.image_frame_bottom, image=photo, anchor=tk.CENTER)
                self.lab.image = photo
                self.lab.pack(side=tk.BOTTOM)

    # Choose Matrix default settings
    def manage_matrix_gene_setting(self, matrix):
        self.mtx_setting_window = self.window
        self.mtx_setting_window.geometry("250x200")
        self.mtx_setting_window.wm_title("Matrix Parameters")
        self.matrix = matrix

        default_setting = ['size_X', 'size_Y', 'spacing_X', 'spacing_Y', 'offset_X', 'offset_Y']
        titre = Label(self.mtx_setting_window, text="Matrix default settings", anchor='w')
        titre.configure(font=("", 16, "bold"))
        titre.place(x=2, y=2)

        width_lab= 15
        width_entry = 13
        setting_frame = Frame(self.mtx_setting_window)
        setting_frame.place(x=2, y=40)
        for row,param in enumerate(default_setting):
            Label(setting_frame, text=matrix.dic[param]['name'], anchor='w', width=width_lab).grid(row=row)
            Entry(setting_frame, textvariable=matrix.dic[param]['tkvar'], width=width_entry).grid(row=row, column=1)
            Label(setting_frame, text=matrix.dic[param]['unit']).grid(row=row, column=2)

        Button(self.mtx_setting_window, text="Validate", command=self.callback_valider_mtx_setting, width=8).place(x=180, y=170)
        Button(self.mtx_setting_window, text="Cancel", comman=self.callback_cancel, width=8).place(x=120, y=170)
    def callback_valider_mtx_setting(self):
        out = self.matrix.check_settings_values()
        if self.matrix.is_settings_valid:
            self.matrix.tk_to_val()
            app.update_mtx_setting(self.matrix)
            self.mtx_setting_window.destroy()
            del self
        else:
            self.raise_error(out)

    # Create a job sequence
    def manage_job(self, job_object, action, penlist, figurelist):
        fig_name_list = [
            "Figure " + str(x.dic["fig_number"]['value'])
            if str(x.dic['fig_custom_name']['value']) == ' '
            else "Figure " + str(x.dic["fig_number"]['value']) + " : " + str(x.dic['fig_custom_name']['value'])
            for x in figurelist ]
        pen_name_list = [
            "Pen " + str(x.dic["pen_number"]['value'])
            if str(x.dic['pen_custom_name']['value']) == ' '
            else "Pen " + str(x.dic["pen_number"]['value']) + " : " + str(x.dic['pen_custom_name']['value'])
            for x in penlist]

        self.job = job_object
        self.manage_job_action = action
        self.penlist = penlist
        self.figurelist = figurelist

        if action == "modify":
            self.job.val_to_tk()

        # Window
        self.new_job_window = self.window
        self.new_job_window.geometry("410x430")
        self.new_job_window.wm_title("Gestion des Jobs")

        # Titre
        titre = Label(self.new_job_window, text="Fenêtre de gestion de Job")
        titre.configure(font=("", 16, "bold"))
        titre.place(x=2, y=2)

        # job setting
        job_setting_frame = Frame(self.new_job_window)
        job_setting_frame.place(x=2,y=40)
        Label(job_setting_frame, text=self.job.dic['job_number']['name'], width=10, anchor='w').grid(row=0)
        Label(job_setting_frame, text=self.job.dic['job_custom_name']['name'],width=10, anchor='w').grid(row=1)
        Entry(job_setting_frame, textvariable=self.job.dic['job_number']['tkvar'],width=15, state=tk.DISABLED).grid(row=0,column=1)
        Entry(job_setting_frame, textvariable=self.job.dic['job_custom_name']['tkvar'], width=15).grid(row=1, column=1)


        # Values entry selector
        job_info_frame = Frame(self.new_job_window, width=400, height=48, bg="#aaaaaa")
        job_info_frame.place(x=2, y=100)
        Label(job_info_frame, text="Order", anchor='w').place(x=2,y=2)
        Label(job_info_frame, text="Select Figure", anchor='w').place(x=44,y=2)
        Label(job_info_frame, text="Select Pen", anchor='w').place(x=224, y=2)
        Entry(job_info_frame, textvariable=self.job.dic['temp_order']['tkvar'], width=5).place(x=2, y=25)
        self.figure_combo_widget = ttk.Combobox(job_info_frame, textvariable=self.job.dic['temp_figure']['tkvar'], width=25)
        self.figure_combo_widget['values'] = fig_name_list
        self.figure_combo_widget['state'] = 'readonly'
        self.figure_combo_widget.place(x=44, y=25)
        self.pen_combo_widget = ttk.Combobox(job_info_frame, textvariable=self.job.dic['temp_pen']['tkvar'], width=25)
        self.pen_combo_widget['values'] = pen_name_list
        self.pen_combo_widget['state'] = 'readonly'
        self.pen_combo_widget.place(x=224, y=25)


        # # TreeView widget init
        treeview_frame = Frame(self.new_job_window, width=405, height=280)
        treeview_frame.place(x=2, y=150)
        self.tree = ttk.Treeview(treeview_frame, selectmode='browse', height=11)
        self.tree.place(x=2, y=2, width=300)
        vsb = ttk.Scrollbar(treeview_frame, orient="vertical", command=self.tree.yview)
        vsb.place(x=300, y=0, height=247)
        self.tree.configure(yscrollcommand=vsb.set)
        Button(treeview_frame, text="Ajouter", command=self.callback_add_sequence, width=10).place(x=320, y=2)
        Button(treeview_frame, text="Supprimer", command=self.callback_delete_sequence_elem, width=10).place(x=320,y=28)
        Button(treeview_frame, text="Annuler", command=self.callback_cancel, width=10).place(x=245, y=252)
        Button(treeview_frame, text="Valider", command=self.callback_valider_job, width=10).place(x=325, y=252)

        if self.manage_job_action == 'modify':
            for ord in self.job.sequence.keys():
                for fig in self.job.sequence[ord].keys():
                    for pen in self.job.sequence[ord][fig].keys():
                        self.callback_add_sequence(ord=ord,fig=fig,pen=pen)
    def callback_add_sequence(self, ord=None, fig=None, pen=None):

        if ord == None and fig == None and pen == None:
            print('add sequence via Button')
            ok, ret = self.job.get_ord_fig_pen()
            if ok == 'error':
                self.raise_error(ret)
                return

            order = ret[0]
            fig_name = ret[1]
            pen_name = ret[2]

            # fill sequence of engrings
            order = str(order)
            fig_number = fig_name.split(" ")
            fig_number = str(fig_number[1])
            pen_number = pen_name.split(" ")
            pen_number = str(pen_number[1])

            # Add order
            if not order in self.job.sequence.keys():
                ord_id = "order" + order
                self.tree.insert('', tk.END, iid=ord_id, text="Rang " + order, open=True)
                self.job.sequence.update({order: {}})

            # Add figure
            if not fig_number in self.job.sequence[order].keys():
                ord_id = "order" + order
                fig_id = "order" + order + "fig" + fig_number

                self.tree.insert('', tk.END, iid=fig_id, text=fig_name, open=True)
                self.tree.move(fig_id, ord_id, 'end')
                self.job.sequence[order].update({fig_number: {}})

            # Add pen
            if not pen_number in self.job.sequence[order][fig_number].keys():
                fig_id = "order" + order + "fig" + fig_number
                pen_id = "order" + order + "fig" + fig_number + "pen" + pen_number

                self.tree.insert('', tk.END, text=pen_name, iid=pen_id, open=True)
                self.tree.move(pen_id, fig_id, 'end')
                self.job.sequence[order][fig_number].update({pen_number: pen_id})

        elif ord != None and fig != None and pen != None:
            print("add sequence via function")
            order = ord
            fig_number = fig
            pen_number = pen
            custom_name_figure = self.job.job_objects["figures"][fig_number].dic['fig_custom_name']['value']
            custom_name_pen = self.job.job_objects["pens"][pen_number].dic['pen_custom_name']['value']

            if custom_name_figure == ' ':
                fig_name = "Figure "+ fig_number
            else:
                fig_name = "Figure "+fig_number+" : "+custom_name_figure
            if custom_name_pen == ' ':
                pen_name = "Pen "+ pen_number
            else:
                pen_name = "Pen "+pen_number+" : "+custom_name_pen

            # Add order
            ord_id = "order" + order
            if not ord_id in self.tree.get_children():
                self.tree.insert('', tk.END, iid=ord_id, text="Rang " + order, open=True)
                self.job.sequence.update({order: {}})

            # Add figure
            ord_id = "order" + order
            fig_id = "order" + order + "fig" + fig_number
            if not fig_id in self.tree.get_children(ord_id):
                self.tree.insert('', tk.END, iid=fig_id, text=fig_name, open=True)
                self.tree.move(fig_id, ord_id, 'end')
                self.job.sequence[order].update({fig_number: {}})

            # Add pen
            fig_id = "order" + order + "fig" + fig_number
            pen_id = "order" + order + "fig" + fig_number + "pen" + pen_number
            if not pen_id in self.tree.get_children(fig_id):
                self.tree.insert('', tk.END, text=pen_name, iid=pen_id, open=True)
                self.tree.move(pen_id, fig_id, 'end')
                self.job.sequence[order][fig_number].update({pen_number: pen_id})
        else:
            print("problème interne : couldn't find the action")
    def callback_delete_sequence_elem(self):
        if len(self.tree.selection())>0:
            selected_item = self.tree.selection()[0]
            self.tree.delete(selected_item)
            ids = selected_item.replace("order","").replace("fig",",").replace("pen",",")
            ids = ids.split(",")
            if len(ids) == 1:
                del self.job.sequence[ids[0]]
            if len(ids) == 2:
                del self.job.sequence[ids[0]][ids[1]]
            if len(ids) == 3:
                del self.job.sequence[ids[0]][ids[1]][ids[2]]
    def callback_valider_job(self):
        out = self.job.check_sequence()
        if self.job.sequence_is_valid:
            self.job.tk_to_val()
            self.job.get_job_objects(self.penlist, self.figurelist)
            app.update_job(self.job, self.manage_job_action)
            self.callback_cancel()
        else:
            self.raise_error(out)

    # Autre
    def callback_cancel(self):
        self.window.destroy()
        del self

    # Errors
    def raise_error(self, err_message):
        messagebox.showerror(title=err_message[0], message=err_message[1])

# class ScrollableFrame(ttk.Frame):
#     def __init__(self, container, h_width=100, v_height=100, *args, **kwargs):
#         super().__init__(container, *args, **kwargs)
#         canvas = tk.Canvas(self, width=h_width, height=v_height)
#         scrollbar = ttk.Scrollbar(self, orient="vertical", command=canvas.yview)
#         self.scrollable_frame = ttk.Frame(canvas)
#
#         self.scrollable_frame.bind(
#             "<Configure>",
#             lambda e: canvas.configure(
#                 scrollregion=canvas.bbox("all")
#             )
#         )
#
#         canvas.create_window((0, 0), window=self.scrollable_frame, anchor="nw")
#
#         canvas.configure(yscrollcommand=scrollbar.set)
#
#         canvas.pack(side="left", fill="both", expand=True)
#         scrollbar.pack(side="right", fill="y")

class Pen():
    def __init__(self, number):

        # parameters
        self.dic = {}
        self.is_valid = False

        # default initialisation
        self.dic = LASER['pen_param']

        for key in self.dic.keys():
            self.dic[key].update({"value": self.dic[key]["default"],
                                  "tkvar": ""})

        for key in self.dic.keys():
            if self.dic[key]["type"] == 'int':
                self.dic[key].update({"tkvar": tk.IntVar(value=self.dic[key]["default"])})
            elif self.dic[key]["type"] == 'string':
                self.dic[key].update({"tkvar": tk.StringVar(value=self.dic[key]["default"])})
            elif self.dic[key]["type"] == 'float':
                self.dic[key].update({"tkvar": tk.DoubleVar(value=self.dic[key]["default"])})
            elif self.dic[key]["type"] == 'bool':
                self.dic[key].update({"tkvar": tk.BooleanVar(value=self.dic[key]["default"])})
            elif self.dic[key]["type"] == 'list':
                self.dic[key].update({"tkvar": tk.StringVar(value=self.dic[key]["default"])})
            else:
                print("problème de type de variable")
        self.dic["pen_number"]["value"] = number
        self.dic['pen_number']["tkvar"].set(number)

    def val_to_tk(self):
        for key in self.dic.keys():
            self.dic[key]["tkvar"].set(self.dic[key]['value'])

    def tk_to_val(self):
        for key in self.dic.keys():
            self.dic[key]["value"] = self.dic[key]["tkvar"].get()

    def check_values(self):
        for key in self.dic:

            # Première vérification pour la valeur
            try:
                tkvar = self.dic[key]["tkvar"].get()
            except:
                traceback.print_exc()
                name = self.dic[key]["name"]
                title = "Paramètre invalide"
                message = "Le paramètre " + str(name) + " possède une valeur non reconnue"
                return title, message

            tktype = self.dic[key]["type"]
            name = self.dic[key]["name"]

            if tktype in ['int', 'float']:
                if type(tkvar) in [type(int()), type(float())]:
                    MIN = self.dic[key]['min']
                    MAX = self.dic[key]['max']
                    if tkvar > MAX or tkvar < MIN:
                       title="Paramètres invalides"
                       message="Le paramètre "+str(name)+" = "+str(tkvar)+" a une valeur erronée.\n\nIntervalle de validité : [" + str(MIN)+"; "+str(MAX)+"]"
                       return title, message

        self.is_valid = True

    def from_file(self, filepath):
        f = open(filepath, 'r')
        content = f.read()
        f.close()

        dic = ast.literal_eval(content)
        for key in self.dic.keys():
            if key != "pen_number":
                self.dic[key]['value'] = dic[key]
        self.val_to_tk()
        out = self.check_values()

        if self.is_valid == False:
            return out

    def from_pen(self, pen_obj):
        """
        This function allow to copy the parameter from a pen to self. We copy every params from the dic and most important, redeclare new tk.Variables because the to pens are diffreent, so the tkvar needs to be stored at different adresses
        :param pen_obj: the source pen used to init the values. This pen is allowed to not be checked when init
        :return: None
        """
        for key in pen_obj.dic.keys():
            if key != 'pen_number':
                if self.dic[key]["type"] == 'int':
                    self.dic[key].update({"tkvar": tk.IntVar(value=pen_obj.dic[key]["value"])})
                elif self.dic[key]["type"] == 'string':
                    self.dic[key].update({"tkvar": tk.StringVar(value=pen_obj.dic[key]["value"])})
                elif self.dic[key]["type"] == 'float':
                    self.dic[key].update({"tkvar": tk.DoubleVar(value=pen_obj.dic[key]["value"])})
                elif self.dic[key]["type"] == 'bool':
                    self.dic[key].update({"tkvar": tk.BooleanVar(value=pen_obj.dic[key]["value"])})
                elif self.dic[key]["type"] == 'list':
                    self.dic[key].update({"tkvar": tk.StringVar(value=pen_obj.dic[key]["value"])})
                else:
                    print("problème de type de variable")

        custom_name = str(self.dic["pen_custom_name"]['tkvar'].get()) + " (from Pen " + str(pen_obj.dic["pen_number"]['value']) + ")"
        self.dic["pen_custom_name"]['tkvar'].set(custom_name)
        self.tk_to_val()

    def to_file(self, buffered):
        out = self.check_values()
        if self.is_valid:
            self.tk_to_val()

            temp_dic = {key: self.dic[key]['value'] for key in self.dic.keys()}
            string_list = str(temp_dic).replace(',', ',\r').replace('{', '{\r ').replace('}', '\r}')

            buffered.write(str(string_list))
            buffered.close()
        else:
            return out

class Figure():
    def __init__(self, number):

        #Type : Object
        self.dic = {}
        self.icon = 0
        self.image = 0
        self.vectors = 0

        # support variables
        self.is_valid = False
        self.canvas_tags = []

        # init figure defaults

        self.dic = APP["figure_param"]
        for key in self.dic.keys():
            self.dic[key].update({"value": self.dic[key]["default"],
                                  "tkvar": ""})

            if self.dic[key]["type"] == "int":
                self.dic[key].update({"tkvar": IntVar(value=self.dic[key]["default"])})
            elif self.dic[key]["type"] == "float":
                self.dic[key].update({"tkvar": DoubleVar(value=self.dic[key]["default"])})
            elif self.dic[key]["type"] == "bool":
                self.dic[key].update({"tkvar": tk.BooleanVar(value=self.dic[key]["default"])})
            elif self.dic[key]["type"] == "string":
                self.dic[key].update({"tkvar": StringVar(value=self.dic[key]["default"])})
            elif self.dic[key]["type"] == 'list':
                self.dic[key].update({"tkvar": tk.StringVar(value=self.dic[key]["default"])})
            else:
                print("problème de paramètres")
        self.dic["fig_number"]["value"] = number
        self.dic['fig_number']["tkvar"].set(number)

        # init galvo param
        self.galvo = LASER["galvo_default"]
        self.galvo_field = self.galvo["galvo_X_max"]['default'] - self.galvo["galvo_X_min"]['default'],\
                            self.galvo["galvo_Y_max"]['default'] - self.galvo["galvo_Y_min"]['default']
        self.galvo_ofst = self.galvo_field[0] - self.galvo["galvo_X_max"]['default'],\
                          self.galvo_field[1] - self.galvo["galvo_Y_max"]['default']

    def from_figure(self, new_figure):
        if new_figure.icon != 0:
            self.icon = new_figure.icon
        if new_figure.image != 0:
            self.image = new_figure.image
        if new_figure.vectors != 0:
            self.vectors = new_figure.vectors

        self.galvo = new_figure.galvo
        self.galvo_field = new_figure.galvo_field
        self.galvo_ofst = new_figure.galvo_ofst

        for key in new_figure.dic.keys():
            if key != 'fig_number':
                if self.dic[key]["type"] == 'int':
                    self.dic[key].update({"tkvar": tk.IntVar(value=new_figure.dic[key]["value"])})
                elif self.dic[key]["type"] == 'string':
                    self.dic[key].update({"tkvar": tk.StringVar(value=new_figure.dic[key]["value"])})
                elif self.dic[key]["type"] == 'float':
                    self.dic[key].update({"tkvar": tk.DoubleVar(value=new_figure.dic[key]["value"])})
                elif self.dic[key]["type"] == 'bool':
                    self.dic[key].update({"tkvar": tk.BooleanVar(value=new_figure.dic[key]["value"])})
                elif self.dic[key]["type"] == 'list':
                    self.dic[key].update({"tkvar": tk.StringVar(value=new_figure.dic[key]["value"])})
                else:
                    print("problème de type de variable")
        custom_name = str(self.dic["fig_custom_name"]['tkvar'].get()) + " (from Figure " + str(
            new_figure.dic["fig_number"]['value']) + ")"
        self.dic["fig_custom_name"]['tkvar'].set(custom_name)
        self.tk_to_val()

    def tk_to_val(self):
        for key in self.dic.keys():
            self.dic[key]["value"] = self.dic[key]["tkvar"].get()

    def val_to_tk(self):
        for key in self.dic.keys():
            self.dic[key]["tkvar"].set(self.dic[key]["value"])

    def import_image(self, filepath):
        if len(filepath) > 1:
            self.image = imgfx.img_import(filepath, to_binary=True)
            self.icon = imgfx.resize_keep_aspect(self.image, 174, 140)

    def check_values(self):
        # Fig_type needs to be defined
        tk_fig_type = self.dic["fig_type"]["tkvar"].get()
        if not tk_fig_type in self.dic["fig_type"]["values"]:
            titre = "Choix du type de figure"
            message = "Veuillez sélectionner un type de Figure : 'image' ou 'vecteur'"
            return titre, message

        if tk_fig_type == 'image':
            if self.image == 0:
                titre = "Aucune image"
                message = "Veuillez importer une image pour générer la figure"
                return titre, message

            image_param = ['fig_number','size_X', 'size_Y', 'position_X', 'position_Y', 'fig_rotation']
            for key in image_param:

                try:
                    tkvar = self.dic[key]["tkvar"].get()
                except:
                    titre = "Valeur inconnue"
                    message = "Le paramètre "+str(self.dic[key]['name'])+" possède une valeur inconnue."
                    return titre, message

                tktype = self.dic[key]["type"]
                name = self.dic[key]['name']

                # Number should be inside their limits
                if tktype in ['int', 'float']:
                    if not type(tkvar) in [type(int()), type(float())]: # is a number
                        if tkvar < self.dic[key]['min'] or tkvar > self.dic[key]["max"]: #is inside limits
                            titre = "Valeur hors limites"
                            message = "Le paramètre "+str(name)+" = "+str(tkvar)+" est hors limite\n\nIntervalle de validité : ["+str(self.dic[key]['min'])+" ; "+str(self.dic[key]['max'])+"]"
                            return titre, message

        elif tk_fig_type == 'vecteur':
            vector_param = ["v_position_X", "v_position_Y", "v_length", "v_angle", "v_repeat_angle", "v_repeat_length", "v_repeat_number"]
            for key in vector_param:
                try:
                    tkvar = self.dic[key]["tkvar"].get()
                except:
                    titre = "Valeur inconnue"
                    message = "Le paramètre "+str(self.dic[key]['name'])+" possède une valeur inconnue."
                    return titre, message

                tktype = self.dic[key]["type"]
                name = self.dic[key]['name']

                # Number should be inside their limits
                if tktype in ['int', 'float']:
                    if not type(tkvar) in [type(int()), type(float())]: # is a number
                        if tkvar < self.dic[key]['min'] or tkvar > self.dic[key]["max"]: #is inside limits
                            titre = "Valeur hors limites"
                            message = "Le paramètre "+str(name)+" = "+str(tkvar)+" est hors limite\n\nIntervalle de validité : ["+str(self.dic[key]['min'])+" ; "+str(self.dic[key]['max'])+"]"
                            return titre, message
        self.is_valid = True

    def get_vectors(self, display, pen=None):
        self.vectors = []
        if display:
            if self.dic['fig_type']['value'] == 'vecteur':
                self.figure_to_display()
            elif self.dic['fig_type']['value'] == 'image':
                self.img_to_display()
        elif display == False and pen != None:
            if self.dic['fig_type']['value'] == 'vecteur':
                self.figure_to_vectors(pen)
            elif self.dic['fig_type']['value'] == 'image':
                self.img_to_vector(pen)
        else:
            print("couldn't get the pen for vectors generation")

    def img_to_display(self):

        hatch = self.galvo_field[0]/(self.dic['canvas_resolution']['value']*2)
        size_X = self.dic['size_X']['value']
        size_Y = self.dic['size_Y']['value']
        position_X = self.dic['position_X']['value']
        position_Y = self.dic['position_Y']['value']
        fig_rotation = self.dic['fig_rotation']['value']

        # img is binary, next is auto_crop into rescale to set the dimention of the cropped image
        image, margin = imgfx.auto_crop(self.image)
        image, trueW_px, trueH_px = imgfx.resize_to_hatch(image, hatch, size_X, size_Y, margin=margin)
        # Image will be flipped
        image = imgfx.img_flip(image)
        # Rotate needs to be done at the end
        image = imgfx.img_rotate(image, fig_rotation)
        # last to binray to delete interpolation residue
        image = imgfx.img_to_binary(image)

        #Mandatory : Calculate the derivative matrix

        mtx_hatch_horiz = np.diff(np.array(image, dtype=np.int), axis=1, append=255)

        start = [ [i,j] if mtx_hatch_horiz[j,i] == -255 else [] for j in range(trueH_px) for i in range(trueW_px)]
        stop = [  [i,j] if mtx_hatch_horiz[j,i] ==  255 else [] for j in range(trueH_px) for i in range(trueW_px)]
        start =[x for x in start if x != [] ]
        stop = [x for x in stop if x != [] ]
        vectors_h = [
            [
                [start[k][0]*hatch + position_X - hatch*trueW_px/2, start[k][1]*hatch + position_Y - hatch*trueH_px/2],
                [stop[k][0]*hatch + position_X - hatch*trueW_px/2, stop[k][1]*hatch + position_Y - hatch*trueH_px/2],
                1
            ]

            for k in range(len(start))
        ]

        mtx_hatch_verti = np.diff(np.array(image, dtype=np.int), axis=0, append=255)

        start = [[i, j] if mtx_hatch_verti[j, i] == -255 else [] for i in range(trueW_px) for j in range(trueH_px)] # this is the list of every mark starting points
        stop = [[i, j] if mtx_hatch_verti[j, i] == 255 else []  for i in range(trueW_px) for j in range(trueH_px)] # This is le list of every mark ending point
        start = [x for x in start if x != []] # Because of "else []" we need to filter the empty points
        stop = [x for x in stop if x != []] # Because of "else []" we need to filter the empty points
        vectors_v = [
            [
                [round(start[k][0] * hatch + position_X - hatch * trueW_px / 2,4),
                 round(start[k][1] * hatch + position_Y - hatch * trueH_px / 2,4)],
                [round(stop[k][0] * hatch + position_X - hatch * trueW_px / 2,4),
                 round(stop[k][1] * hatch + position_Y - hatch * trueH_px / 2,4)],
                1
            ]

            for k in range(len(start)) # Parcourir les point de start (et de stop puisqu'ils ont la même longueur)
        ]


        self.vectors = vectors_h + vectors_v

    def img_to_vector(self, pen):

        # Pen params
        hatch = pen.dic['hatch']['value']
        hatch_angle = pen.dic['hatch_angle']['value']
        hatching_pattern = pen.dic['hatching_pattern']['value']
        nb_pass = pen.dic['nb_passage']['value']
        dbh = pen.dic['double_hatch']['value']

        # Get figure param
        position_X = self.dic['position_X']['value']
        position_Y = self.dic['position_Y']['value']
        fig_rotation = self.dic['fig_rotation']['value']
        size_X = self.dic['size_X']['value']
        size_Y = self.dic['size_Y']['value']

        # BEGIN GENERATION
        # img is binary, next is auto_crop into rescale to set the dimention of the cropped image
        image, margin = imgfx.auto_crop(self.image)
        image, trueW_px, trueH_px = imgfx.resize_to_hatch(image, hatch, size_X, size_Y, margin=margin)
        # Image will be flipped
        image = imgfx.img_flip(image)
        # Rotate needs to be done at the end
        image = imgfx.img_rotate(image, fig_rotation - hatch_angle)
        # last to binray to delete interpolation residue
        image = imgfx.img_to_binary(image)

        # add special dev for lasers
        if 'engraving_delay' in pen.dic.keys():
            delay = pen.dic['engraving_delay']['value']
        if "hatch_regulate" in pen.dic.keys():
            hatchreg = pen.dic['hatch_regulate']['value']
            hatchreg_delay = pen.dic['pathreg_delay']['value']

        # Mandatory : Calculate the derivative matrix
        mtx_hatch_horiz = np.diff(np.array(image, dtype=np.int), axis=1, prepend=255)

        # Hatching pattern S should invert every 2 row the direction of the vectors
        if hatching_pattern == 'S':
            # masking_h = np.ones(image.shape, dtype=np.int)
            # masking_h[::2, :] = -1
            # masking_v = np.ones(image.shape, dtype=np.int)
            # masking_v[:, ::2] = -1
            #
            # mtx_hatch_horiz = np.multiply(mtx_hatch_horiz,masking_h)

            # start and stop are the vectors beginning points and FINISHING POINTS
            print("WORK IN PROGRESS")

        elif hatching_pattern == 'Z':
            print("hatching_pattern is Z")
            start = [[x, y] if mtx_hatch_horiz[y, x] == -255 else []
                     for y in range(trueH_px)
                     for x in range(trueW_px)]
            stop = [[x, y] if mtx_hatch_horiz[y, x] == 255 else []
                    for y in range(trueH_px)
                    for x in range(trueW_px)]

            # filter empty points
            start = [x for x in start if x != []]
            stop = [x for x in stop if x != []]

            # fill vector
            vectors_h = [
                [
                    [round(start[k][0] * hatch + position_X - hatch * trueW_px / 2, 4),
                     round(start[k][1] * hatch + position_Y - hatch * trueH_px / 2, 4)],
                    [round(stop[k][0] * hatch + position_X - hatch * trueW_px / 2, 4),
                     round(stop[k][1] * hatch + position_Y - hatch * trueH_px / 2, 4)],
                    1
                ]
                for k in range(len(start))
            ]

            if dbh:
                print("is double hatched")
                mtx_hatch_verti = np.diff(np.array(image, dtype=np.int), axis=0, prepend=255)

                start = [[i, j] if mtx_hatch_verti[j, i] == -255 else []
                         for i in range(trueW_px)
                         for j in range(trueH_px)]
                stop = [[i, j] if mtx_hatch_verti[j, i] == 255 else []
                        for i in range(trueW_px)
                        for j in range(trueH_px)]

                # filter empty values
                start = [x for x in start if x != []]
                stop = [x for x in stop if x != []]
                vectors_v = [
                    [
                        [round(start[k][0] * hatch + position_X - hatch * trueW_px / 2,4),
                         round(start[k][1] * hatch + position_Y - hatch * trueH_px / 2,4)],
                        [round(stop[k][0] * hatch + position_X - hatch * trueW_px / 2,4),
                         round(stop[k][1] * hatch + position_Y - hatch * trueH_px / 2,4)],
                        1
                    ]
                    for k in range(len(start))
                ]

                self.vectors = vectors_h + vectors_v
                return

            self.vectors = vectors_h
        else:
            print("un recognizeed hatching pattern value :", hatching_pattern)
            self.vectors = []

    def figure_to_display(self):
        # Récupération des variables
        N = self.dic["v_repeat_number"]['value']
        start_x0, start_y0 = self.dic["v_position_X"]['value'], self.dic["v_position_Y"]['value']
        hyp = self.dic["v_length"]["value"]
        angle = self.dic['v_angle']['value']
        repeat_angle = self.dic["v_repeat_angle"]["value"]
        if self.dic['v_repeat_length']['value'] < self.galvo_field[0]/(self.dic["canvas_resolution"]["value"]*2):
            length = self.galvo_field[0]/(self.dic["canvas_resolution"]["value"]*2)
        else:
            length = self.dic['v_repeat_length']['value']

        origin = [self.galvo_ofst[0], self.galvo_ofst[1]]

        jump_inc_x = length * np.cos(repeat_angle * np.pi / 180)
        jump_inc_y = length * np.sin(repeat_angle * np.pi / 180)
        mark_inc_x = hyp * np.cos(angle * np.pi / 180)
        mark_inc_y = hyp * np.sin(angle * np.pi / 180)

        start_mark_points = [[round(start_x0 + i * jump_inc_x,4),
                              round(start_y0 + i * jump_inc_y,4)]
                             for i in range(N)]
        stop_mark_points = [[round(point[0] + mark_inc_x,4),
                             round(point[1] + mark_inc_y,4)]
                            for point in start_mark_points]

        max_x = np.max(np.array(start_mark_points + stop_mark_points)[:, 0])
        max_y = np.max(np.array(start_mark_points + stop_mark_points)[:, 1])
        min_x = np.min(np.array(start_mark_points + stop_mark_points)[:, 0])
        min_y = np.min(np.array(start_mark_points + stop_mark_points)[:, 1])
        self.dic['size_X']['value'] = max_x - min_x
        self.dic['size_Y']['value'] = max_y - min_y

        vectors = [[start_mark_points[0], stop_mark_points[0], 1]]

        for i in range(1, len(start_mark_points)):
            vectors.append([vectors[-1][1], start_mark_points[i], 0])  # Jump between

            vectors.append([start_mark_points[i], stop_mark_points[i], 1])  # Mark

        self.vectors = vectors

    def figure_to_vectors(self,pen):
        # Récupération des variables
        N = self.dic["v_repeat_number"]['value']
        start_x0, start_y0 = self.dic["v_position_X"]['value'], self.dic["v_position_Y"]['value']
        hyp = self.dic["v_length"]["value"]
        angle = self.dic['v_angle']['value']
        repeat_angle = self.dic["v_repeat_angle"]["value"]
        length = self.dic["v_repeat_length"]["value"]

        origin = [self.galvo_ofst[0], self.galvo_ofst[1]]

        jump_inc_x = length * np.cos(repeat_angle * np.pi / 180)
        jump_inc_y = length * np.sin(repeat_angle * np.pi / 180)
        mark_inc_x = hyp * np.cos(angle * np.pi / 180)
        mark_inc_y = hyp * np.sin(angle * np.pi / 180)

        start_mark_points = [[round(start_x0 + i * jump_inc_x,4),
                              round(start_y0 + i * jump_inc_y,4)]
                             for i in range(N)]
        stop_mark_points = [[round(point[0] + mark_inc_x,4),
                             round(point[1] + mark_inc_y,4)]
                            for point in start_mark_points]

        max_x = np.max(np.array(start_mark_points + stop_mark_points)[:, 0])
        max_y = np.max(np.array(start_mark_points + stop_mark_points)[:, 1])
        min_x = np.min(np.array(start_mark_points + stop_mark_points)[:, 0])
        min_y = np.min(np.array(start_mark_points + stop_mark_points)[:, 1])
        self.dic['size_X']['value'] = max_x - min_x
        self.dic['size_Y']['value'] = max_y - min_y

        vectors = [[start_mark_points[0], stop_mark_points[0], 1]]

        for i in range(1, len(start_mark_points)):
            vectors.append([vectors[-1][1], start_mark_points[i], 0])  # Jump between

            vectors.append([start_mark_points[i], stop_mark_points[i], 1])  # Mark

        self.vectors = vectors


class Job():
    def __init__(self, number):
        self.sequence = {}
        self.dic = {}
        self.sequence_is_valid = False
        self.job_objects = {"figures": {}, 'pens': {}}
        self.job = {}
        self.tree_view = 0
        self.is_matrixable = False


        self.dic = APP['job_param']

        for key in self.dic.keys():
            self.dic[key].update({"value": self.dic[key]["default"],
                                  "tkvar": ""})

        # initiate tkvars
        for key in self.dic.keys():
            if self.dic[key]["type"] == 'int':
                self.dic[key].update({"tkvar": tk.IntVar(value=self.dic[key]["default"])})
            elif self.dic[key]["type"] == 'string':
                self.dic[key].update({"tkvar": tk.StringVar(value=self.dic[key]["default"])})
            elif self.dic[key]["type"] == 'float':
                self.dic[key].update({"tkvar": tk.DoubleVar(value=self.dic[key]["default"])})
            elif self.dic[key]["type"] == 'bool':
                self.dic[key].update({"tkvar": tk.BooleanVar(value=self.dic[key]["default"])})
            elif self.dic[key]["type"] == 'list':
                self.dic[key].update({"tkvar": tk.StringVar(value=self.dic[key]["default"])})
            else:
                print("problème de type de variable")

        self.dic["job_number"]["value"] = number
        self.dic['job_number']["tkvar"].set(number)

    def tk_to_val(self):
        for key in self.dic.keys():
            self.dic[key]["value"] = self.dic[key]["tkvar"].get()

    def val_to_tk(self):
        for key in self.dic.keys():
            if self.dic[key]["type"] == 'int':
                self.dic[key]["tkvar"] = tk.IntVar(value=self.dic[key]["value"])
            elif self.dic[key]["type"] == 'string':
                self.dic[key]["tkvar"] = tk.StringVar(value=self.dic[key]["value"])
            elif self.dic[key]["type"] == 'float':
                self.dic[key]["tkvar"] = tk.DoubleVar(value=self.dic[key]["value"])
            elif self.dic[key]["type"] == 'bool':
                self.dic[key]["tkvar"] = tk.BooleanVar(value=self.dic[key]["value"])
            elif self.dic[key]["type"] == 'list':
                self.dic[key]["tkvar"] = tk.StringVar(value=self.dic[key]["value"])
            else:
                print("problème de type de variable")

    def get_ord_fig_pen(self):
        try:
            self.tk_to_val()
        except:
            err_mess = "Mauvaise Valeur", "Une des valeurs est mauvaise"
            return 'error', err_mess
        order = int(self.dic["temp_order"]['value'])
        fig_name = self.dic['temp_figure']['value']
        pen_name = self.dic['temp_pen']['value']

        if order < 1:
            err_mess = "Rank error", "Rank must be greater than 0"
            return 'error', err_mess
        if fig_name == ' ':
            err_mess = "Figure error", "A figure number must be provided"
            return 'error', err_mess
        if pen_name == ' ':
            err_mess = "Pen error", "A pen must be provided"
            return 'error', err_mess

        return 'ok', (order, fig_name, pen_name)

    def check_sequence(self):
        if len(self.sequence.keys()) == 0:
            titre = "Rank is missing"
            message = "Please add a rank to the sequence"
            self.sequence_is_valid = False
            return titre, message

        for ord in self.sequence.keys():
            if len(self.sequence[ord].keys()) == 0:
                titre = "Figure is missing"
                message = "Please add a figure to the rank "+ord
                self.sequence_is_valid = False
                return titre, message

            for fig in self.sequence[ord].keys():
                if len(self.sequence[ord][fig]) == 0:
                    titre = "Pen is missing"
                    message = "Please add a Pen to the figure "+fig
                    self.sequence_is_valid = False
                    return titre, message
        norm_ord = [str(i) for i in range(1,len(self.sequence.keys())+1)]

        if norm_ord != list(self.sequence.keys()):
            new_dic = {str(key): self.sequence[key] for key in sorted(self.sequence)}
            self.sequence = new_dic


        self.sequence_is_valid = True

        length = 0
        for ord in self.sequence.keys():
            for fig in self.sequence[ord].keys():
                for pen in self.sequence[ord][fig].keys():
                    length+=1
        if length == 1:
            self.is_matrixable = True

    def get_job_objects(self, penlist, figurelist):
        if self.sequence_is_valid == False:
            print("La sequence n'est pas déclarée")
            return
        figure_numbers = [list(self.sequence[x].keys()) for x in self.sequence.keys()]
        figure_numbers =  list(dict.fromkeys([y for x in figure_numbers for y in x]))
        pen_numbers = [list(self.sequence[x][y].keys()) for x in self.sequence.keys() for y in self.sequence[x].keys()]
        pen_numbers = list(dict.fromkeys([y for x in pen_numbers for y in x]))

        self.job_objects["pens"] = {str(num): pen
                                    for num in pen_numbers
                                    for pen in penlist if pen.dic['pen_number']['value'] == int(num) }
        self.job_objects["figures"] = {str(num): fig
                                    for num in figure_numbers
                                    for fig in figurelist if fig.dic['fig_number']['value'] == int(num)}

    def figure_to_vector(self):

        print("begin generation")
        for ord in self.sequence.keys():
            for fig in self.sequence[ord].keys():

                figure = self.job_objects['figures'][fig]

                if figure.dic["fig_type"]['value'] == 'image':
                    for p in self.sequence[ord][fig].keys():
                        pen = self.job_objects['pens'][p]
                        t0 = time.time()
                        figure.get_vectors(False, pen)
                        t1 = time.time()
                        self.sequence[ord][fig][p] = {"id":"ord"+ord+"fig"+fig+"pen"+p,
                                                      "pen_object": pen,
                                                      "figure_object" : figure,
                                                      "generation_time_s": round(t1-t0,3),
                                                      "number_of_vectors": len(figure.vectors)}
                        print("\nid","ord"+ord+"fig"+fig+"pen"+p)
                        print("generation_time_s", round(t1-t0,3))
                        print("number_of_vectors", len(figure.vectors))


                elif figure.dic["fig_type"]['value'] == 'vecteur':
                    for p in self.sequence[ord][fig].keys():
                        print("\t\tpen", p)
                        pen = self.job_objects['pens'][p]
                        t0 = time.time()
                        figure.get_vectors(False, pen)
                        t1 = time.time()
                        self.sequence[ord][fig][p] = {"id":"ord"+ord+"fig"+fig+"pen"+p,
                                                      "pen_object": pen,
                                                      "figure_object" : figure,
                                                      "generation_time_s": round(t1-t0,3),
                                                      "number_of_vectors": len(figure.vectors)}
                        print("\nid","ord"+ord+"fig"+fig+"pen"+p)
                        print("generation_time_s", round(t1-t0,3))
                        print("number_of_vectors", len(figure.vectors))


                else:
                    print("unrecognized fig_type value :", figure.dic["fig_type"]['value'])

class Matrix():
    def __init__(self):

        # parameter
        self.dic = {}
        self.is_valid = False
        self.is_settings_valid = False
        self.job = None

        # default init
        self.dic = APP['matrix_gene_default']

        for key in self.dic.keys():
            self.dic[key].update({"value": self.dic[key]["default"],
                                  "tkvar": ""})

        for key in self.dic.keys():
            if self.dic[key]["type"] == 'int':
                self.dic[key].update({"tkvar": tk.IntVar(value=self.dic[key]["default"])})
            elif self.dic[key]["type"] == 'string':
                self.dic[key].update({"tkvar": tk.StringVar(value=self.dic[key]["default"])})
            elif self.dic[key]["type"] == 'float':
                self.dic[key].update({"tkvar": tk.DoubleVar(value=self.dic[key]["default"])})
            elif self.dic[key]["type"] == 'bool':
                self.dic[key].update({"tkvar": tk.BooleanVar(value=self.dic[key]["default"])})
            elif self.dic[key]["type"] == 'list':
                self.dic[key].update({"tkvar": tk.StringVar(value=self.dic[key]["default"])})
            else:
                print("problème de type de variable")

    def check_settings_values(self):
        for key in self.dic.keys():
            if self.dic[key]['is_default_setting']:
                try:
                    tkvar = self.dic[key]["tkvar"].get()
                except:
                    titre = "Valeur inconnue"
                    message = "Le paramètre "+str(self.dic[key]['name'])+" possède une valeur inconnue."
                    return titre, message

                tktype = self.dic[key]["type"]
                name = self.dic[key]['name']

                # Number should be inside their limits
                if tktype in ['int', 'float']:
                    if not type(tkvar) in [type(int()), type(float())]:  # is a number
                        if tkvar < self.dic[key]['min'] or tkvar > self.dic[key]["max"]:  # is inside limits
                            titre = "Valeur hors limites"
                            message = "Le paramètre " + str(name) + " = " + str(
                                tkvar) + " est hors limite\n\nIntervalle de validité : [" + str(
                                self.dic[key]['min']) + " ; " + str(self.dic[key]['max']) + "]"
                            return titre, message
                self.is_settings_valid = True

    def check_values(self):
        if self.is_settings_valid == False:
            titre = "Réglages invalides"
            message = "Les réglages par défaut de la matrice sont mauvais"
            return titre, message

        for key in self.dic.keys():
            if self.dic[key]['is_default_setting'] == False:
                # Are values of the right type
                try:
                    tkvar = self.dic[key]["tkvar"].get()
                except:
                    titre = "Valeur inconnue"
                    message = "Le paramètre " + str(self.dic[key]['name']) + " possède une valeur inconnue."
                    return titre, message

                tktype = self.dic[key]["type"]
                name = self.dic[key]['name']

                # Number should be inside their limits
                # if tktype in ['int', 'float']:
                #     if type(tkvar) in [type(int()), type(float())]:  # is a number
                #         original_param_min = Pen(0).dic[self.dic['selected_horiz_param']['value']]['min']
                #         original_param_max = Pen(0).dic[self.dic['selected_horiz_param']['value']]['max']
                #         if tkvar < original_param_min or tkvar > original_param_max:
                #             titre = "Valeur hors limites"
                #             message = "Le paramètre " + str(name) + " = " + str(
                #                 tkvar) + " est hors limite\n\nIntervalle de validité : [" + str(
                #                 self.dic[key]['min']) + " ; " + str(self.dic[key]['max']) + "]"
                #             return titre, message


                # Number of param < 256
                if self.dic['nb_value_horiz_param']['tkvar'].get() * self.dic['nb_value_verti_param']['tkvar'].get() > 255:
                    titre = "Pen Overload"
                    message = "Le nombre de Pen devant être généré excède le nombre de pen maximal (MAX = 255)."
                    return titre, message

                if self.dic["nb_value_horiz_param"]['tkvar'].get() == 0 and self.dic["nb_value_verti_param"]['tkvar'].get() == 0:
                    titre = "Null Matrix"
                    message = "Veuillez donner une dimension à la matrice. Dimension minimum = 1x1"
                    return titre, message

        # Check inside param limits

    def tk_to_val(self, is_setting=None):
        if is_setting == None:
            for key in self.dic.keys():
                self.dic[key]["value"] = self.dic[key]["tkvar"].get()
        for key in self.dic.keys():
            if self.dic[key]['is_default_setting'] == is_setting:
                self.dic[key]["value"] = self.dic[key]["tkvar"].get()

    def generate_objects(self, penlist, figurelist):
        # Horizontal has to be generated
        if len(penlist) > 0:
            app.callback_remove_all_pen()

        #Get the maximal index of figure list
        figure_numbers = [figurelist[i].dic['fig_number']['value'] for i in range(len(figurelist))]
        max_figure_count = max(figure_numbers)

        # Get the selected figure id to get the figure
        fig_list_temp = self.dic['fig_selection']['value']
        selected_figure = int(fig_list_temp.replace("Figure ", '')[0])
        figure = [figurelist[i] for i in range(len(figurelist)) if
                  figurelist[i].dic['fig_number']['value'] == selected_figure]
        figure = figure[0]

        mtx_ofst_x = self.dic['offset_X']['value']
        mtx_ofst_y = self.dic['offset_Y']['value']
        mtx_size_x = self.dic['size_X']['value']
        mtx_size_y = self.dic['size_Y']['value']
        mtx_step_x = self.dic['spacing_X']['value']
        mtx_step_y = self.dic['spacing_Y']['value']
        fig_size_x = figure.dic['size_X']['value']
        fig_size_y = figure.dic['size_Y']['value']
        # every measures are in the same direction top left corner
        elem0_pos = mtx_ofst_x - mtx_size_x / 2, mtx_ofst_y + mtx_size_y / 2
        elem_step = fig_size_x + mtx_step_x, fig_size_y + mtx_step_y

        # Get the values of horizontal axis
        if self.dic['nb_value_horiz_param']['value'] != 0:
            param_name_horiz = self.dic['selected_horiz_param']['value']
            pen = Pen(0)
            fig = Figure(0)
            if param_name_horiz in pen.dic.keys():
                MIN = self.dic['selected_horiz_param_min']['value']
                MAX = self.dic['selected_horiz_param_max']['value']
                NB = self.dic['nb_value_horiz_param']['value']
                values_horiz = [MIN + i*(MAX-MIN)/NB
                                for i in range(1,NB+1)
                                if elem0_pos[0] + (i-1)*elem_step[0] <= mtx_size_x]

                if len(penlist) > 0:
                    app.callback_remove_all_pen()

            elif param_name_horiz in fig.dic.keys():
                print("param belong to figure")
                MIN = self.dic['selected_horiz_param_min']['value']
                MAX = self.dic['selected_horiz_param_max']['value']
                NB = self.dic['nb_value_horiz_param']['value']
                values_horiz = [MIN + i * (MAX - MIN) / NB
                                for i in range(1, NB + 1)
                                if elem0_pos[0] + (i-1)*elem_step[0] <= mtx_size_x]

            else:
                print("Problème de reconnaissance de paramètre horizontal :", param_name_horiz)
                values_horiz = [" "]
                param_name_horiz = ""
        else:
            values_horiz = [" "]
            param_name_horiz = ""

        # Get the values of vertical axis
        if self.dic['nb_value_verti_param']['value'] != 0:
            param_name_verti = self.dic['selected_verti_param']['value']
            pen = Pen(0)
            fig = Figure(0)
            if param_name_verti in pen.dic.keys():
                MIN = self.dic['selected_verti_param_min']['value']
                MAX = self.dic['selected_verti_param_max']['value']
                NB = self.dic['nb_value_verti_param']['value']
                values_verti = [MIN + i * (MAX - MIN) / NB
                                for i in range(1, NB + 1)
                                if elem0_pos[1] + (i-1)*elem_step[1] <= mtx_size_y]

            elif param_name_verti in fig.dic.keys():
                print("param belong to figure")
                MIN = self.dic['selected_verti_param_min']['value']
                MAX = self.dic['selected_verti_param_max']['value']
                NB = self.dic['nb_value_verti_param']['value']
                values_verti = [MIN + i * (MAX - MIN) / NB
                                for i in range(1, NB + 1)
                                if elem0_pos[1] + (i-1)*elem_step[1] <= mtx_size_y]

            else:
                print("Problème de reconnaissance de paramètre :", param_name_verti)
                values_verti = [" "]
                param_name_verti = ""
        else:
            values_verti = [" "]
            param_name_verti = ""

        new_fig_count = 1
        for i, val_verti in enumerate(values_verti):
            for j,val_horiz in enumerate(values_horiz):

                # Generate Figures
                temp_fig = Figure(0)
                temp_fig.from_figure(figure, max_figure_count + new_fig_count)
                temp_fig.dic['fig_custom_name']['value'] = "("+param_name_horiz+" = "+str(val_horiz)+" ; "+param_name_verti+" = "+str(val_verti)+")"
                temp_fig.dic['v_position_X']['value'] = elem0_pos[0] + j * elem_step[0]
                temp_fig.dic['v_position_Y']['value'] = elem0_pos[1] + i * elem_step[1]

                temp_fig.get_vectors()
                temp_fig.val_to_tk()

                # Generate Pens
                temp_pen = Pen(new_fig_count)
                temp_pen.dic['pen_custom_name']['value'] = "(" + param_name_horiz + " = " + str(val_horiz) + " ; " + param_name_verti + " = " + str(val_verti) + ")"

                # choose if selected param belongs to Figure
                if param_name_horiz in temp_fig.dic.keys():
                    temp_fig.dic[param_name_horiz]['value'] = val_horiz
                if param_name_verti in temp_fig.dic.keys():
                    temp_fig.dic[param_name_verti]['value'] = val_verti

                # Choose if the selected param belongs to Pen
                need_new_pen = False
                if param_name_horiz in temp_pen.dic.keys():
                    temp_pen.dic[param_name_horiz]['value'] = val_horiz
                    need_new_pen = True
                if param_name_verti in temp_pen.dic.keys():
                    temp_pen.dic[param_name_verti]['value'] = val_verti
                    need_new_pen = True

                # Append the appropriate Object
                app.update_figure(temp_fig, 'add') # always add figure
                if need_new_pen:
                    app.update_pen(temp_pen, 'add') # add pen only id needed

                new_fig_count += 1

class Application(Frame):

    # Initialisation du contenu de la fenetre principale
    def __init__(self, parent):
        #Mandatory commands
        Frame.__init__(self, parent)
        self.desktop_path = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')

        # Variables
        self.pen_list_count = 0
        self.penlist = []
        self.figure_list_count = 0
        self.figurelist = []
        self.joblist = []
        self.tk_job_list = tk.Variable(value=self.joblist)
        self.joblist_count = 0
        self.laser_is_ready_to_engrave = False

        self.create_default_widgets()

    def create_default_widgets(self):
        self.pen_frame = Frame(self, bg='#3E7300', width=350, height=335)
        self.figure_frame = Frame(self, bg='#3D73B8', width=350, height=335)
        self.previsualise_frame = Frame(self, bg='#3D00B8', width=500, height=500)
        self.matrix_generation_frame = Frame(self, bg='#FF00B8', width=500, height=160)
        self.communication_frame = Frame(self, bg='#FBB0B8', width=350, height=200)
        self.job_sequence_frame = Frame(self, bg="#0B11B8", width=350, height=463)

        self.pen_frame.place(x=20, y=20)
        self.figure_frame.place(x=20, y=370)
        self.previsualise_frame.place(x=390, y=200)
        self.matrix_generation_frame.place(x=390, y=20)
        self.communication_frame.place(x=910, y=500)
        self.job_sequence_frame.place(x=910, y=20)

        self.create_pen_frame()
        self.create_figure_frame()
        self.create_matrix_generator_frame()
        self.create_previsualisation_frame()
        self.create_job_sequence_frame()
        self.create_communication_frame()

    # Concerning pen frame
    def create_pen_frame(self):
        Label(self.pen_frame, text="Liste des pens").place(x=2, y=2)
        Button(self.pen_frame, text="+", width=2, command=self.callback_add_pen_window).place(x=250, y=0)
        Button(self.pen_frame, text="-", width=2, command=self.callback_remove_pen).place(x=275, y=0)
        Button(self.pen_frame, text="-all", width=2, command=self.callback_remove_all_pen).place(x=300, y=0)
        Button(self.pen_frame, text="m", width=2, command=self.callback_modify_pen_window).place(x=325, y=0)

        self.penlist_widget = tk.Listbox(self.pen_frame, width=57, height=19, selectmode='browse')
        self.penlist_widget.yview_scroll(1,'units')
        self.penlist_widget.place(x=2, y=25)
        self.penlist_widget.bind('<Double-Button-1>', self.callback_modify_pen_window)
        self.penlist_widget.bind('<Button-2>', self.clear_pen_selection)
        self.penlist_widget.bind('<Button-3>', self.clear_pen_selection)
    def callback_add_pen_window(self):
        """
        When the + is clicked the user has to options.
        1 - There is no selection in the widget. This means the user wants to create a pen with default values, a brand new pen
        2 - There is one pen selected in the widget. This means the user wants to create a new pen but with the values of the one selected. This can ease the pen creation

        In the option 2 we need to create a new Pen number but with values of the on which is selected

        This function opens the popup (tkinter.Toplevel) for parameters input
        :return: None
        """

        number = self.get_free_pen_number_value()  # get the first pen number available in the penlist

        #Create pen from scratch
        if len(self.penlist_widget.curselection()) == 0:
            new_pen = Pen(number)
            Popup(root).manage_pen(new_pen, "add")
            return new_pen
        elif len(self.penlist_widget.curselection()) == 1:
            new_pen = Pen(number)
            selected_pen = self.penlist[self.penlist_widget.curselection()[0]]
            new_pen.from_pen(selected_pen)
            Popup(root).manage_pen(new_pen, "add")
            return new_pen
    def callback_remove_pen(self):
        if len(self.penlist_widget.curselection()) == 1:
            self.penlist.pop(self.penlist_widget.curselection()[0])
            self.penlist_widget.delete(self.penlist_widget.curselection())
            self.pen_list_count -= 1
            print('REMOVE 1 PEN')
    def callback_remove_all_pen(self):
        out = messagebox.askquestion('Supprimer tous les Pens', 'Etes-vous sure de vouloir supprimer tous les pen ?',
                                           icon='warning')
        if out == 'yes':
            self.penlist = []
            self.penlist_widget.delete(0,tk.END)
            self.pen_list_count = 0
            print("REMOVE ALL PENS")
    def callback_modify_pen_window(self, event=None):
        """
        Modify pen can only work if 1 pen is selected.
        :param event: inherited from the .bind() function. This param is the handler.
        :return: None
        """
        if len(self.penlist_widget.curselection()) == 1:
            temp_pen = self.penlist[self.penlist_widget.curselection()[0]]
            Popup(root).manage_pen(temp_pen, "modify")
            self.temp_select_pen = self.penlist_widget.curselection()[0]
    def update_pen(self, in_pen, action):
        if in_pen.dic["pen_custom_name"]['value'] == " ":
            display_name = "Pen "+str(in_pen.dic['pen_number']['value'])
        else:
            display_name = "Pen " + str(in_pen.dic['pen_number']['value']) + " : " + str(in_pen.dic["pen_custom_name"]["value"])

        if action == "modify":
            self.penlist[self.temp_select_pen] = in_pen
            self.penlist_widget.delete(self.temp_select_pen)
            self.penlist_widget.insert(self.temp_select_pen, display_name)
            print('MODIFY PEN')
        elif action == 'add':
            self.penlist_widget.insert(self.pen_list_count, display_name)
            self.penlist.append(in_pen)
            self.pen_list_count += 1
            print("ADD PEN")
    def get_free_pen_number_value(self):
        pens_number = [pen.dic['pen_number']['value'] for pen in self.penlist]
        number = 1
        while number in pens_number:
            number += 1
        return number
    def clear_pen_selection(self,e):
        self.penlist_widget.selection_clear(0, tk.END)

    # concerning figure frame
    def create_figure_frame(self):
        Label(self.figure_frame, text="Liste des figures").place(x=2, y=2)
        Button(self.figure_frame, text="+", width=2, command=self.callback_add_figure_window).place(x=250, y=0)
        Button(self.figure_frame, text="-", width=2, command=self.callback_remove_1figure).place(x=275, y=0)
        Button(self.figure_frame, text="-all", width=2, command=self.callback_remove_all_figure).place(x=300, y=0)
        Button(self.figure_frame, text="m", width=2, command=self.callback_modify_figure_window).place(x=325, y=0)

        self.figure_list_widget = tk.Listbox(self.figure_frame, width=57, height=19, selectmode='browse')
        self.figure_list_widget.yview_scroll(1,'units')
        self.figure_list_widget.place(x=2, y=25)
        self.figure_list_widget.bind('<Double-Button-1>', self.callback_modify_figure_window)
        self.figure_list_widget.bind('<Button-2>', self.clear_figure_selection)
        self.figure_list_widget.bind('<Button-3>', self.clear_figure_selection)
    def callback_add_figure_window(self):
        number = self.get_free_figure_number_value()
        if len(self.figure_list_widget.curselection()) == 0:
            new_figure = Figure(number)
            Popup(root).manage_figure(new_figure, "add")
        elif len(self.figure_list_widget.curselection()) == 1:
            new_figure = Figure(number)
            selected_figure = self.figurelist[self.figure_list_widget.curselection()[0]]
            new_figure.from_figure(selected_figure)
            Popup(root).manage_figure(new_figure, "add+")
    def callback_remove_1figure(self):
        if len(self.figure_list_widget.curselection()) == 1:

            #Undo canvas
            temp_fig = self.figurelist[self.figure_list_widget.curselection()[0]]
            for t in temp_fig.canvas_tags:
                self.canvas.delete(t)

            self.figurelist.pop(self.figure_list_widget.curselection()[0])
            self.figure_list_widget.delete(self.figure_list_widget.curselection())
            self.figure_list_count -= 1
            print("REMOVE 1 FIGURE")
    def callback_remove_all_figure(self):
        out = messagebox.askquestion('Supprimer toutes les Figures', 'Etes-vous sure de vouloir supprimer toutes les Figures ?',
                                     icon='warning')
        if out == 'yes':
            # Undo canvas
            for fig in self.figurelist:
                for t in fig.canvas_tags:
                    self.canvas.delete(t)

            self.figurelist = []
            self.figure_list_widget.delete(0, tk.END)
            self.figure_list_count = 0
            print("REMOVE ALL FIGURES")
    def callback_modify_figure_window(self,e=None):
        if len(self.figure_list_widget.curselection()) == 1:
            temp_fig = self.figurelist[self.figure_list_widget.curselection()[0]]
            Popup(root).manage_figure(temp_fig, "modify")
            self.temp_select_figure = self.figure_list_widget.curselection()[0]
    def update_figure(self, figure, action):
        if figure.dic["fig_custom_name"]['value'] == " ":
            display_name = "Figure "+str(figure.dic['fig_number']["value"])
        else:
            display_name = "Figure " + str(figure.dic['fig_number']["value"])+" : "+figure.dic["fig_custom_name"]['value']

        if action == "add":
            self.figure_list_widget.insert(tk.END, "loading ...")
            self.update_idletasks()
            
            figure.get_vectors(display=True)
            self.figure_list_widget.delete(tk.END)
            self.figure_list_widget.insert(self.figure_list_count, display_name)
            self.figure_list_count += 1
            self.figurelist.append(figure)
            print('ADD FIGURE')

        if action == "add+":
            self.figure_list_widget.insert(tk.END, "loading ...")
            self.update_idletasks()

            figure.get_vectors(display=True)
            self.figure_list_widget.delete(tk.END)
            self.figure_list_widget.insert(self.figure_list_count, display_name)
            self.figure_list_count += 1
            self.figurelist.append(figure)
            print('ADD FIGURE')

        elif action == "modify":
            self.figure_list_widget.delete(self.temp_select_figure)
            self.figure_list_widget.insert(self.temp_select_figure, "loading ...")
            figure.get_vectors(display=True)
            self.figurelist[self.temp_select_figure] = figure
            self.figure_list_widget.delete(self.temp_select_figure)
            self.figure_list_widget.insert(self.temp_select_figure, display_name)


            # Undo canvas
            for t in figure.canvas_tags:
                self.canvas.delete(t)
            self.draw_figure(figure)
            print("MODIFY FIGURE")
    def get_free_figure_number_value(self):
        figure_numbers = [fig.dic['fig_number']['value'] for fig in self.figurelist]
        number = 1
        while number in figure_numbers:
            number += 1
        return number
    def clear_figure_selection(self, e):
        self.figure_list_widget.selection_clear(0, tk.END)

    #Concerning Matrix generation
    def create_matrix_generator_frame(self):
        def get_pen_fig_param():
            pen = Pen(0)
            figure = Figure(0)

            param_list_p = [x for x in pen.dic.keys() if Pen(0).dic[x]['allow_mtx_gene']]
            param_list_f = [x for x in figure.dic.keys() if Figure(0).dic[x]['allow_mtx_gene']]

            return param_list_p + param_list_f

        self.mtx = Matrix()

        #self.matrix_generation_frame
        Label(self.matrix_generation_frame, text="Génération de matrices").place(x=2, y=2)
        Button(self.matrix_generation_frame, text="Settings", command=self.matrix_gene_setting).place(x=400, y=0)
        Button(self.matrix_generation_frame, text="Generate",command=self.callback_generate_matrix).place(x=410, y=128)
        Button(self.matrix_generation_frame, text="Refresh", command=self.callback_refresh_job_list).place(x=350, y=32)
        # Figure select
        job_select_frame = Frame(self.matrix_generation_frame)
        Label(job_select_frame, text=self.mtx.dic["job_selection"]['name']).grid(row=0)
        self.job_selector = ttk.Combobox(job_select_frame, textvariable=self.mtx.dic['job_selection']['tkvar'], width=40)
        self.job_selector['state'] = 'readonly'
        self.job_selector.grid(row=0, column=1)
        job_select_frame.place(x=2, y=30)

        #Parameters select
        param_select_frame = Frame(self.matrix_generation_frame)
        param_select_frame.place(x=2, y=55)
        Label(param_select_frame, text="Paramètre", anchor='w', width=15).grid(row=0,column=1)
        Label(param_select_frame, text="MIN", anchor='w').grid(row=0, column=2)
        Label(param_select_frame, text="MAX", anchor='w').grid(row=0, column=3)
        Label(param_select_frame, text="Nombre", anchor="w").grid(row=0, column=4)
        # Horizontal
        Label(param_select_frame, text=self.mtx.dic['selected_horiz_param']['name'], anchor='w', width=15).grid(row=1)
        combobox_horizontal = ttk.Combobox(param_select_frame, textvariable=self.mtx.dic['selected_horiz_param']['tkvar'], width=15)
        combobox_horizontal['values'] = get_pen_fig_param()
        combobox_horizontal['state'] = 'readonly'
        combobox_horizontal.grid(row=1,column=1, padx=(15, 15))
        Entry(param_select_frame, textvariable=self.mtx.dic["selected_horiz_param_min"]['tkvar'], width=5).grid(row=1, column=2)
        Entry(param_select_frame, textvariable=self.mtx.dic["selected_horiz_param_max"]['tkvar'], width=5).grid(row=1, column=3)
        Entry(param_select_frame, textvariable=self.mtx.dic["nb_value_horiz_param"]['tkvar'], width=8).grid(row=1, column=4, padx=(15, 15))

        # Vertical
        Label(param_select_frame, text=self.mtx.dic['selected_verti_param']['name'], anchor='w', width=15).grid(row=2)
        combobox_vertical = ttk.Combobox(param_select_frame, textvariable=self.mtx.dic['selected_verti_param']['tkvar'], width=15)
        combobox_vertical['values'] = get_pen_fig_param()
        combobox_vertical['state'] = 'readonly'
        combobox_vertical.grid(row=2, column=1, padx=(15, 15))
        Entry(param_select_frame, textvariable=self.mtx.dic["selected_verti_param_min"]['tkvar'], width=5).grid(row=2, column=2)
        Entry(param_select_frame, textvariable=self.mtx.dic["selected_verti_param_max"]['tkvar'], width=5).grid(row=2, column=3)
        Entry(param_select_frame, textvariable=self.mtx.dic["nb_value_verti_param"]['tkvar'], width=8).grid(row=2,column=4,padx=(15, 15))
    def get_job_list(self):
        temp = ["Job " + str(x.dic['job_number']['value'])
                if x.dic['job_custom_name']['value'] == " "
                else "Job " + str(x.dic['job_number']['value']) + " : " + str(x.dic['job_custom_name']['value'])
                for x in self.joblist]
        self.tk_job_list.set(temp)
        self.job_selector['values'] = self.tk_job_list.get()
    def matrix_gene_setting(self):
        Popup(root).manage_matrix_gene_setting(self.mtx)
    def update_mtx_setting(self, matrix):
        self.mtx = matrix
    def callback_refresh_job_list(self):
        temp = ["Job " + str(x.dic['job_number']['value'])
                if x.dic['job_custom_name']['value'] == " "
                else "Job " + str(x.dic['job_number']['value']) + " : " + str(x.dic['job_custom_name']['value'])
                for x in self.joblist]
        self.job_selector['values'] = temp
    def callback_generate_matrix(self):
        print("begin generation")
        self.mtx.check_settings_values()
        out = self.mtx.check_values()
        self.mtx.tk_to_val() # get the param, number, min, max, values

        self.mtx.generate_objects(self.penlist, self.figurelist)

    #Concerning Previsualize frame
    def create_previsualisation_frame(self):
        Label(self.previsualise_frame, text="Prévisualisation").place(x=2, y=2)
        self.canvas_frame = Frame(self.previsualise_frame, background="green", width=460, height=460, highlightbackground="black", highlightthickness=1)
        self.canvas_frame.place(x=19, y=30)
        Button(self.previsualise_frame, text="+", command=self.draw_figure, width=2).place(x=350, y=0)
        Button(self.previsualise_frame, text="+all", command=self.draw_multiple_figure, width=2).place(x=375, y=0)
        Button(self.previsualise_frame, text="-", command=self.undraw_figure, width=2).place(x=400, y=0)
        Button(self.previsualise_frame, text="-all", command=self.undraw_all_figure, width=2).place(x=425,y=0)
        Button(self.previsualise_frame, text="zr", command=self.reset_zoom, width=2).place(x=450, y=0)
        self.canvas = Canvas(self.previsualise_frame, width=460, height=460)
        self.canvas.place(x=19, y=30)
        self.canvas.bind("<MouseWheel>", self.do_zoom)
        self.canvas.bind('<ButtonPress-1>', lambda event: self.canvas.scan_mark(event.x, event.y))
        self.canvas.bind("<B1-Motion>", lambda event: self.canvas.scan_dragto(event.x, event.y, gain=1))
        self.temp_factor = 1
        self.origX = self.canvas.xview()[0]
        self.origY = self.canvas.yview()[0]


        # Draw repere lines
        self.canvas.create_line(0, 230, 460, 230, fill='black', dash=(1,5))
        self.canvas.create_line(230, 0, 230, 460, fill='black', dash=(1,5))
        self.canvas.create_line(0, 0, 460, 0, fill='black')
        self.canvas.create_line(460, 0, 460, 460, fill='black')
        self.canvas.create_line(460, 460, 0, 460, fill='black')
        self.canvas.create_line(0, 460, 0, 0, fill='black')

    def draw_multiple_figure(self, list_index=[]):
        for ele in list_index:
            temp_fig = self.figurelist[ele]
            self.draw_figure(temp_fig)

        for fig in self.figurelist:
            self.draw_figure(fig)
    def undraw_figure(self, figure=None):
        if figure == None:
            if len(self.figure_list_widget.curselection()) != 0:
                index = self.figure_list_widget.curselection()[0]
                figure = self.figurelist[index]
                # Undo canvas
                for t in figure.canvas_tags:
                    self.canvas.delete(t)
    def undraw_all_figure(self):
        # Undo canvas
        for figure in self.figurelist:
            for t in figure.canvas_tags:
                self.canvas.delete(t)
    def draw_figure(self, figure=None):
        if figure == None:
            if len(self.figure_list_widget.curselection()) == 0:
                return
            index = self.figure_list_widget.curselection()[0]
            figure = self.figurelist[index]

        # Draw figure vectors
        vectors = figure.vectors
        for i,vec in enumerate(vectors):
            [x0, y0], [x1, y1], on = vec[0], vec[1], vec[2]
            ratioX, ratioY = 460/figure.galvo_field[0], 460/figure.galvo_field[1]
            x0 = round((x0 + figure.galvo_ofst[0])*ratioX)
            x1 = round((x1 + figure.galvo_ofst[0])*ratioX)
            y0 = round((figure.galvo_ofst[1] - y0)*ratioY) # invert Y axis beacause a canvas has Y axis pointing down
            y1 = round((figure.galvo_ofst[1] - y1)*ratioY) # invert Y axis beacause a canvas has Y axis pointing down

            if on == 0:
                tag_value = 'jump'+str(figure.dic['fig_number']['value']) + str(i)
                self.canvas.create_line(x0, y0, x1, y1, fill='grey', tags=tag_value)
                figure.canvas_tags.append(tag_value)
            elif on == 1:
                tag_value = 'mark' + str(figure.dic['fig_number']['value']) + str(i)
                self.canvas.create_line(x0, y0, x1, y1, fill='red', tags=tag_value)
                figure.canvas_tags.append(tag_value)

        # Draw repere lines
        self.canvas.create_line(0, 230, 460, 230, fill='black', dash=(1,5))
        self.canvas.create_line(230, 0, 230, 460, fill='black', dash=(1,5))
    def do_zoom(self, event):
        x = self.canvas.canvasx(event.x)
        y = self.canvas.canvasy(event.y)
        factor = 1.001 ** event.delta
        self.temp_factor *= factor
        self.canvas.scale(ALL, x, y, factor, factor)
    def reset_zoom(self):
        self.canvas.xview_moveto(self.origX)
        self.canvas.yview_moveto(self.origY)
        self.canvas.scale(ALL, 230,230,1/self.temp_factor, 1/self.temp_factor)
        self.temp_factor = 1

    # Concerning Job frame
    def create_job_sequence_frame(self):
        Label(self.job_sequence_frame, text="Jobs").place(x=2, y=2)
        Button(self.job_sequence_frame, text="+", width=2, command=self.callback_add_job_window).place(x=250, y=0)
        Button(self.job_sequence_frame, text="-", width=2, command=self.callback_remove_1_job).place(x=275, y=0)
        Button(self.job_sequence_frame, text="-all", width=2, command=self.remove_all_job).place(x=300, y=0)
        Button(self.job_sequence_frame, text="m", width=2, command=self.callback_modify_job_window).place(x=325, y=0)

        self.job_sequence_widget = tk.Listbox(self.job_sequence_frame, width=57, height=25, selectmode = "multiple")
        self.job_sequence_widget.yview_scroll(1, 'units')
        self.job_sequence_widget.place(x=2, y=25)
    def get_free_job_number_value(self):
        job_numbers = [job.dic['job_number']['value'] for job in self.joblist]
        number = 1
        while number in job_numbers:
            number += 1
        return number
    def callback_add_job_window(self):
        number = self.get_free_job_number_value()
        temp_job = Job(number)
        print("add job number", number)
        print(self.joblist)
        Popup(root).manage_job(temp_job, "add", self.penlist, self.figurelist)

    def callback_modify_job_window(self):
        if len(self.job_sequence_widget.curselection()) == 1:
            temp_job = self.joblist[self.job_sequence_widget.curselection()[0]]
            self.temp_select_job = self.job_sequence_widget.curselection()[0]
            Popup(root).manage_job(temp_job, "modify", self.penlist, self.figurelist)
            print("MODIFY JOB")
            self.get_job_list()
    def callback_remove_1_job(self):
        if len(self.job_sequence_widget.curselection()) == 1:
            del self.joblist[self.job_sequence_widget.curselection()[0]]
            self.job_sequence_widget.delete(self.job_sequence_widget.curselection())
            self.joblist_count -= 1
            print("REMOVE 1 JOB")
            self.get_job_list()
            return
        elif len(self.job_sequence_widget.curselection()) > 1:
            for val in self.job_sequence_widget.curselection():
                self.job_sequence_widget.delete(val)
                del self.joblist[val]
                self.joblist_count -= 1
        print("REMOVE MULTIPLE JOB")
        self.get_job_list()
    def remove_all_job(self):
        out = messagebox.askquestion('Supprimer tous les Jobs',
                                     'Etes-vous sure de vouloir supprimer tous les Jobs ?',
                                     icon='warning')
        if out == 'yes':
            self.joblist = []
            self.job_sequence_widget.delete(0, tk.END)
            self.joblist_count = 0
            print("REMOVE ALL JOBS")
            self.get_job_list()
    def update_job(self, job, action):
        print("arriving in update job", job.dic['job_number']["value"])
        if job.dic["job_custom_name"]['value'] == " ":
            display_name = "Job " + str(job.dic['job_number']["value"])
        else:
            display_name = "Job " + str(job.dic['job_number']["value"]) + " : " + job.dic["job_custom_name"]['value']

        if action == "add":
            self.job_sequence_widget.insert(self.joblist_count, display_name)
            self.joblist_count += 1
            self.joblist.append(job)
            print("after append", job.dic['job_number']['value'])
            print('ADD JOB')

        elif action == "modify":
            self.job_sequence_widget.delete(self.temp_select_job)
            self.joblist[self.temp_select_job] = job
            self.job_sequence_widget.insert(self.temp_select_job, display_name)
            print('MODIFY JOB')

        self.get_job_list()





    # COncerning Communication frame for CO2
    def create_communication_frame(self):
        if LASER['id'] == "co2_taufenbach":
            self.fill_co2_communication_frame()


    # Concerning Communication with co2 taufenbach
    def fill_co2_communication_frame(self):
        self.laser_connect = tk.StringVar(value="Status not checked")
        self.laser_status = tk.StringVar(value=" ")
        self.laser_ping = tk.StringVar(value=" ")
        self.keep_pinging = False
        self.keep_status = False

        # Save as Button
        Button(self.communication_frame, text="Export as CNC", width=10, command=self.callback_save_as_CNC).place(x=2,y=2)

        # Laser specific communication
        connect_frame = tk.Frame(self.communication_frame, width=345, height=60)
        connect_frame.place(x=2, y=32)
        self.laser_connect_label = tk.Label(connect_frame, textvariable=self.laser_connect, width=25, anchor='w')
        self.laser_connect_label.place(x=70, y=2)
        tk.Button(connect_frame, text="Refresh", command=self.callback_refresh_laser_status, width=8).place(x=0, y=0)
        tk.Label(connect_frame, text="ping :", anchor='w').place(x=250, y=2)
        self.keep_reading_label = tk.Label(connect_frame, textvariable=self.laser_ping, width=6, anchor='w')
        self.keep_reading_label.place(x=285, y=2)
        self.ping_thread = threading.Thread(group=None, target=self.thread_keep_pinging)
        tk.Label(connect_frame, text="ready : ", anchor='w').place(x=250, y=32)
        self.keep_status_label = tk.Label(connect_frame, textvariable=self.laser_status, width=6, anchor='w')
        self.keep_status_label.place(x=290, y=32)
        self.status_thread = threading.Thread(group=None, target=self.thread_keep_laser_status)

        # Print_frame
        print_frame = tk.Frame(self.communication_frame, width=345, height=65)
        print_frame.place(x=2, y=96)
        self.bt_print_job = tk.Button(print_frame, text="Print \nJob", command=self.callback_print_job, width=8, height=3, state=tk.DISABLED).place(x=275, y=5)
        self.bt_print_cnc = tk.Button(print_frame, text="Print \nfrom CNC", command=self.callback_print_from_cnc, width=8, height=3, state=tk.DISABLED).place(x=215, y=5)
        self.bt_stop = tk.Button(print_frame, text="Stop", command=self.callback_stop_engraving, width=8, height=3, state=tk.DISABLED).place(x=120, y=5)
        self.bt_relaunch = tk.Button(print_frame, text="Relaunch", command=self.callback_stop_engraving, width=8, height=3, state=tk.DISABLED).place(x=60, y=5)
    def callback_refresh_laser_status(self):
        self.laser_connect.set("Try to connect...")
        self.laser_connect_label.config(fg="Orange")
        self.update_idletasks()
        self.keep_pinging = False
        self.keep_status = False
        time.sleep(0.5)  # Wait for the threads to die

        self.laser_obj = co2.Laser()

        if not self.laser_obj.connect():
            self.laser_is_ready_to_engrave = False
            self.laser_connect.set("Laser does not communicate")
            self.laser_connect_label.config(fg="Red")
            self.update_idletasks()
            return

        self.laser_is_ready_to_engrave = False
        self.keep_pinging = True
        self.keep_status = True
        self.laser_connect.set("Laser is connected")
        self.laser_connect_label.config(fg="Green")
        self.ping_thread.start()
        self.status_thread.start()
    def callback_save_as_CNC(self):
        if len(self.job_sequence_widget.curselection()) == 1:
            print('ONE job selected')

            f = filedialog.asksaveasfile(title="Save Job as CNC",
                                         mode='w',
                                         defaultextension=".cnc",
                                         filetypes=[('CNC', '.cnc')],
                                         initialdir=os.path.expanduser(APP['paths']["user_document"]))
            if f is None:  # asksaveasfile return `None` if dialog closed with "cancel".
                return

            job = self.joblist[self.job_sequence_widget.curselection()[0]]
            job.figure_to_vector()

            cnc = co2.CNC(job)
            # take the cnc export path default value for save as
            ret, err = cnc.save_as_file(f.name)
            if ret:
                messagebox.showinfo(title="CNC saved", message="CNC successfully saved at\n\n"+str(f.name))
            else:
                messagebox.showerror(title="CNC exportation failed",message=err)
    def callback_print_job(self):
        if len(self.job_sequence_widget.curselection()) == 1:
            print('SEND FROM JOB')

            # Get job
            job = self.joblist[self.job_sequence_widget.curselection()[0]]
            job.figure_to_vector()

            if LASER['id'] == "co2_taufenbach":

                # Create CNC directly from job
                cnc = co2.CNC(job)
                default_cnc_filepath = os.path.expanduser(APP['paths']['user_document']) + "/default.cnc"
                cnc.save_as_file(default_cnc_filepath)

                # send the CNC
                self.laser_obj = co2.Laser()
                self.laser_obj.send(default_cnc_filepath)
                print("object sent")
                self.laser_obj.print(default_cnc_filepath, 0) # last param is galvo rotation
    def callback_print_from_cnc(self):
        print("SEND FROM FILE")
        f = filedialog.askopenfile(title="Open CNC",
                                   mode='r',
                                   defaultextension=".cnc",
                                   filetypes=[('CNC', '.cnc')],
                                   initialdir=APP['paths']['user_document'])
        if f is None:  # asksaveasfile return `None` if dialog closed with "cancel".
            return

        filepath_to_cnc = f.name
        self.laser_obj = co2.Laser()
        self.laser_obj.send(filepath_to_cnc)
        self.laser_obj.print(filepath_to_cnc, 0)  # last param is galvo rotation
    def callback_stop_engraving(self):
        print('stop')
        self.laser_obj.stop()
    def callback_relaunch(self):
        self.laser_obj.relaunch()
    def thread_keep_pinging(self):
        while self.keep_pinging:
            out, ms = self.laser_obj.ping()
            if out:
                self.laser_ping.set(str(ms)+ " ms")
            else:
                self.laser_ping.set("x")
            self.update_idletasks()
    def thread_keep_laser_status(self):
        while self.keep_status:
            if self.laser_obj.check_status():
                self.laser_is_ready_to_engrave = True
                self.laser_status.set("True")
                self.keep_status_label.config(fg="SpringGreen")
                self.bt_stop.configure['state'] = tk.ACTIVE
                self.bt_print_cnc.configure['state'] = tk.ACTIVE
                self.bt_print_job.configure['state'] = tk.ACTIVE
                self.bt_relaunch.configure['state'] = tk.ACTIVE
            else:
                self.laser_is_ready_to_engrave = False
                self.laser_status.set("False")
                self.keep_status_label.config(fg="red")
                self.bt_stop.configure['state'] = tk.DISABLED
                self.bt_print_cnc.configure['state'] = tk.DISABLED
                self.bt_print_job.configure['state'] = tk.DISABLED
                self.bt_relaunch.configure['state'] = tk.DISABLED
            self.update_idletasks()

    # Support functions
    def raise_error(self, code):
        message = ""
        if code == "pin overload":
            message = "Nombre de pens trop grand. (MAX = 256)"
            messagebox.showwarning(title="Pens Overload", message=message)





try:
    # Selcet a configuration
    if DEBUG == False:
        LASER = App_config()
    else:
        f = open(ROOT + "/Lasers/CO2_Taufenbach/laser_config.txt", 'r')
        content = f.read()
        f.close()
        LASER = ast.literal_eval(content)
        co2.CONFIG = LASER


    # Window parameter
    root = Tk()
    root.geometry("1280x720")
    root.resizable(False, False)
    root.title("Engraving Master")

    app = Application(root)
    app.pack(side="top", fill="both", expand=True)
    if DEBUG:
        temp_pen = Pen(1)
        app.update_pen(temp_pen,"add")
        temp_fig = Figure(1)
        app.update_figure(temp_fig, "add")
    app.mainloop()

except:
    print("\n\n\n######## SEND THE FOLLOWING MESSAGE TO TECHNICAL SUPPORT #####\n\n")
    traceback.print_exc()
    print("##########################################################")
    input("Appuyer sur ENTRER pour terminer")
    sys.exit(0)
