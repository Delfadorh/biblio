from PIL import Image, ImageOps

def img_import(filepath, to_binary=False):
    if to_binary == False:
        return  Image.open(filepath)
    else:
        # norm : background = white, engraving = black
        img = Image.open(filepath)
        gray = ImageOps.grayscale(img)
        img_range = gray.getextrema()
        threshold = round((img_range[1] - img_range[0]) / 2)

        tru_gray = gray.convert("L")
        thresh = tru_gray.point(lambda p: p < threshold and 255)
        inv_thresh = ImageOps.invert(thresh)
        return inv_thresh

def resize_keep_aspect(img, max_w_px, max_h_px):

    xx, yy = img.size

    if yy < xx:
        width = max_w_px
        height = int(width * yy / xx)
        rescaled = img.resize((width, height))
    elif yy > xx:
        height = max_h_px
        width = int(height * xx / yy)
        rescaled = img.resize((width, height))
    else:
        rescaled = img.resize((max_w_px, max_h_px))

    return rescaled

def img_rotate(img, angle):
    xx, yy = img.size

    rot = img.rotate(angle, expand=True, resample=Image.BILINEAR, fillcolor =(255))

    return rot

def img_flip(img):
    flipped = img.transpose(Image.FLIP_TOP_BOTTOM)
    return flipped

def img_to_binary(src):
    # norm : background = white, engraving = black
    gray = ImageOps.grayscale(src)
    img_range = gray.getextrema()
    threshold = round((img_range[1]-img_range[0])/2)
    tru_gray = gray.convert("L")
    thresh = tru_gray.point(lambda p: p > threshold and 255)

    return thresh

def auto_crop(src, margin=5):
    inv = ImageOps.invert(src)
    bbox = inv.getbbox() # x0, y0, x1, y1
    reinv = ImageOps.invert(inv)
    cropped_img = src.crop(bbox)

    width, height = cropped_img.size
    new_width = width + 2*margin
    new_height = height + 2*margin
    result = Image.new(cropped_img.mode, (new_width, new_height), 255)
    result.paste(cropped_img, (margin, margin))

    return result, margin

def resize_to_hatch(src, hatch, trueW, trueH, margin=5):
    xx,yy = src.size
    truxx, truyy = xx-2*margin, yy-2*margin

    truW_px = trueW/hatch # le nombre de pixel que doit faire l'image
    trueH_px = trueH/hatch

    truW_px_m = int(round(truW_px + 2*margin))
    trueH_px_m = int(round(trueH_px + 2*margin))

    resized = resize_keep_aspect(src, truW_px_m, trueH_px_m)

    realW_px, realH_px = resized.size

    return resized, realW_px, realH_px

def order_jumplines(length, jump):
    raw_list = [i for i in range(length)]
    max_val = max(raw_list)
    lines_list = []
    it = 0
    ofst = 0

    while len(raw_list) > 0:
        lines_list.append(it)
        raw_list.remove(it)

        it += jump
        if it > max_val:
            ofst += 1
            it = ofst

    return lines_list