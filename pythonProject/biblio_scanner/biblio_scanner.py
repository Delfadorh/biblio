import os
from PyPDF2 import PdfFileReader
from PyPDF2 import PdfFileWriter


def get_all_files(path):
    print("Extracting all files from", path)
    file_list = []
    for root, dirs, files in os.walk(path, topdown=False):
        for name in files:
            file_list.append(os.path.join(root, name))

    return file_list

def filter_folder(file_list, string_to_filter):
    print("Drops :", string_to_filter)
    for filt in string_to_filter:
        file_list = [file for file in file_list if not filt in file]

    return file_list

def shorten_path(files):
    char_to_drop = ""
    for i, letter in enumerate(files[0]):
        flag = False
        for file in files:
            if file[i] == letter:
                flag = True
            else:
                print("Common string path", char_to_drop)
                temp_files = [{"relative_path" : path.replace(char_to_drop,""), "raw_path": path} for path in files]
                return temp_files

        if flag == True:
            char_to_drop+= letter





############ MAIN #############
biblio_path = os.getcwd().replace("\\pythonProject\\biblio_scanner","")
filter_list = [r"\.git", r"\pythonProject", r"\Mesures"]
files = get_all_files(biblio_path)
files = filter_folder(files, filter_list)
files = shorten_path(files)


for file in files:
    if '.pdf' in file:
        print(file)
        document = PdfFileReader(open(file["raw_path"], 'rb'))
        print(document.getDocumentInfo())







        input('next')